# Heimdall

## Key requirements
- Accept alert payloads from Prometheus, Logstash and extracted from email (maybe within the router or via a separate application - separate keeps the router scope small).
- Checks the alert against a set of business rules to decide how to route them; to automated remediation, chatops, email, alerta?  All of the above?
- Maintains state in order to allow routing to change based on how many times an alert has been seen in a defined period.
- Clear state based on business logic. For example we want to clear state if the alert has not been seen again in 36 hours.
- Maintain a history in order to be able to report on alerts which are continually coming up.  With not all alerts actually making it to Alerta, the router is likely our source of truth for frequency.
- Allow business rules to be written by people who are not programmers; so likely in a DSL using YAML?

With the routing engine being the central point that alerts flow through, it should also be the place where we define silences.

# Routing business rule configuration
With the business rules needing to be able to be written and maintained by people who are not programmers, it is important we have a simple(ish) DSL.

````yaml
# Friendly name for the alert; if the alert is coming from somewhere like AlertManager, this name should match the alert definition there for ease of management.
# Convention is that this config file is named the same as the friendly name; all lowercase with spaces replaced by _.
name: 'Metrics Unavailable'

# Criteria to match the alert on.  All fields are mandatory.
# Each parameter value is checked using Regex, so you have the full power of regex at your disposal.  Ensure you test your regex; you can use https://regex101.com/ for this purpose.
# Matches are always case-insensitive.
match:
    type: 'Metrics Unavailable'
    subject: '.*'
    service: '.*'
    system: '.*'
    environment: '.*'
    host: '.*'
    source: '.*'
    resource: '.*'

# Link to the runbook for this alert.  All alerts MUST have a corresponding runbook which provides further information and explains how to proceed.
runbook: 'https://wiki/issues/metrics_unavailable'

# Actions to take when alert fires.
#
# Recurrence is the number of times that the alert has been seen in a defined time period.  The time period is defined by the 'clear_after' parameter which deletes the recurrence counter after the time period specified after the last seen event.
# If recurrence is omitted the action will fire every time the alert is seen.
#
# Each action allows for some metadata to be changed.
#   For example an alert may initially just be an information event, but if it has been seen x times you may wish to increase the severity to warning.
#   You can also add a message to the alert providing further information as to why it has been raised, and an indication of what to do next.
#
# Handlers takes an array to allow for multiple outputs.  See https://xxxxxxx for more information on available handlers and their parameters
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "vro"
                parameters:
                    workflow: "8sdf98a7yf89afy089"
                    service: "Telegraf"
                    host: "xxx"
    -
        recurrence: 2
        severity: 'warning'
        message: 'Metrics continue to be unavailable despite attempting to restart the Telegraf service.  This means that real issues may be missed because we are not collecting metrics.'
        handlers:
            -
                action: 'rocketchat'
                channel: 'xxxx'
            -
                action: 'alerta'

# The time period after the last seen alert at which the recurrence counter should be cleared.
# Valid units are (d)ays, (h)ours or (m)inutes.
clear_after: 1d
````

Not all business logic will be able to be expressed via the DSL, it would get somewhat complicated to support that, so we need to allow an action which 'runs a fancy function' to perform the business logic.
