FROM golang:alpine as builder

ENV VERSION="0.18.0"

ARG UID=10000
ARG GID=10001

WORKDIR $GOPATH/src/gitlab.com/rokett
RUN \
    apk add --no-cache git && \
    addgroup --g $GID heimdalld && \
    adduser --disabled-password --no-create-home --shell /sbin/nologin --uid $UID --ingroup heimdalld heimdalld && \
    git clone --branch $VERSION --depth 1 https://gitlab.com/rokett/heimdall.git heimdall && \
    cd heimdall && \
    BUILD=$(git rev-list -1 HEAD) && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -mod=vendor -ldflags "-X main.version=$VERSION -X main.build=$BUILD -s -w -extldflags '-static'" -o heimdalld ./cmd/heimdalld

FROM scratch
LABEL maintainer="rokett@rokett.me"
COPY --from=builder /go/src/gitlab.com/rokett/heimdall/heimdalld /
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Run with non-root user
USER heimdalld

EXPOSE 10002

ENTRYPOINT ["./heimdalld"]
