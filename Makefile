export CGO_ENABLED = 0
export GOOS = linux
export GOARCH = amd64

VERSION := 0.18.0
BUILD := $(shell git rev-list -1 HEAD)
LDFLAGS := -ldflags "-X main.version=$(VERSION) -X main.build=$(BUILD) -s -w -extldflags '-static'"

build:
	go build -a -mod=vendor $(LDFLAGS) -o heimdalld ./cmd/heimdalld

docker:
	docker build --no-cache -t rokett/heimdall:v$(VERSION) .
	docker push rokett/heimdall:v$(VERSION)

publish: docker
	docker build --no-cache -t rokett/heimdall:latest .
	docker push rokett/heimdall:latest

outdated:
	go list -u -m -mod=mod -json all | ~/go-mod-outdated -update -direct
