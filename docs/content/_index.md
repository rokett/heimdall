---
title: ""
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

Heimdall is an event processor and forwarder.  Essentially it sits between event generating systems and event action systems (think alerting dashboards or automation systems for example).

As a concept, an event is not the same as an alert; it is only an alert if you decide it should be an alert.  How you surface that alert is then up to how you define your rule handlers.  Before it is an alert, other things may happen to an event; maybe the first time an event is seen it is ignored, or some automation process is kicked off to automatically remediate the event.

That is all Heimdall does.  Events come in from supported systems, they are processed according to rules you define, and then they are spat out the other side to the handler defined within the rule.

![High level flow](../high_level_flow.png "High level flow")
