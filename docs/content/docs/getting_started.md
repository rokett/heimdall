---
title: "Getting Started"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

![High level flow](../../high_level_flow.png "High level flow")

You should probably read about the [architecture](/architecture) before starting things up, it won't take long and will fill you in on how to make things resilient.  The guide below will get you started with a single server, **non-resilient**, setup though.

# Pre-requisites
Heimdall requires a database; currently only MS SQL Server is supported.

# Let's go!
1. Create a blank database. The default name is `heimdall` but you can call it whatever you want and pass the name in to the server using a flag, `--db`, if you wish.
2. Create a local database user with read/write access to the database. The username and password are passed to the server using the `--db_username` and `--db_password` flags respectively.
3. You’ll probably want to run Heimdall as a service, in which case you need to use something like [NSSM](https://nssm.cc/).
4. Run the executable in order to create the database struture and validate configuration.  Here's an example command.

```
heimdalld.exe --db_username "heimdall" --db_password "my-strong-password"
```

5. Using the `PUT /rules` endpoint, upload the default rule.  A sample payload is as follows:

```json
{
    "name": "Default Heimdall rule",
    "match": {
        "event_type": ".*?",
        "subject": ".*?",
        "service": ".*?",
        "system": ".*?",
        "environment": ".*?",
        "host": ".*?",
        "source": ".*?",
        "resource": ".*?"
    },
    "runbook": "https://wiki.url/issues/default_heimdall_rule",
    "actions": [
        {
            "recurrence": 1,
            "handlers": [
                {
                    "action": "alerta",
                    "parameters": {
                        "url": "http://alerta.url"
                    }
                }
            ]
        }
    ],
    "clear_recurrence_after": "1h"
}
```

And that is pretty much it.  Heimdall will now be listening for incoming events on http://url/event.  Sending a payload to the `event` endpoint itself will require a [Heimdall native payload]({{< relref "input_processors" >}}).  To send an event from Alertmanager, send it to http://url/event?source=alertmanager.

Make sure you review the [Output Handlers]({{< relref "output_handlers" >}}) docs to see what other flags may need to be set for the handlers you want to use.

# Removing rules
Rules can be removed by sending a `DELETE` request to the `/rules` endpoint.  The payload is simply the name of the rule.

```json
{
    "name": "Default Heimdall rule"
}
```

# Configuring the server

The following flags are available for configuring the server.

| Flag                     | Description                                                                                | Default Value  | Required?                              |
| ------------------------ | ------------------------------------------------------------------------------------------ | -------------- | -------------------------------------- |
| `log_file`               | Name of file to log Heimdall events to                                                     | Log to console | No                                     |
| `port`                   | The port that the server will listen on for API and web requests                           | 10002          | No                                     |
| `db_host`                | The IP address of the database host, including instance name if needed                     | 127.0.0.1      | No                                     |
| `db_port`                | Port to connect to database server on                                                      | 1433           | No                                     |
| `db`                     | Name of the database to use                                                                | heimdall       | No                                     |
| `db_username`            | Username of account with read/write access to database                                     |                | Yes - If not using integrated security |
| `db_password`            | Password for database user                                                                 |                | Yes - If not using integrated security |
| `email_host`             | The SMTP host used to send email if the email handler is used                              |                | No                                     |
| `email_port`             | The TCP port to use to access the SMTP host                                                | 25             | No                                     |
| `email_from_address`     | From address for email sent from Heimdall                                                  |                | No                                     |
| `auth_tokens`            | Comma separated list of event handler authentication tokens in key=value format            |                | No                                     |
| `tracing.endpoint`       | Tracing endpoint in the format IP:Port                                                     |                | No                                     |
| `tracing.secure`         | Set to true if the tracing endpoint supports TLS connections                               | false          | No                                     |
| `deployment.environment` | The environment this application is running under; generally either `test` or `production` |                | Yes                                    |
| `http.request_timeout`   | The number of seconds before the incoming request will timeout                             | 5              | No                                     |
| `version`                | Display the version number only                                                            | false          | No                                     |

# Monitoring
The service status can be checked by sending a `GET` request to the `/status` route.  A status code of `200` means that all is ok.  This is the endpoint to use for load balancing healthchecks.

Metrics are available in Prometheus format by sending a `GET` request to the `/metrics` route.
