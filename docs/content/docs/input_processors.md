---
title: "Input processors"
weight: 3
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

An input processor defines a format for the incoming event.  In some cases, such as for [Alertmanager](https://github.com/prometheus/alertmanager) and [vRealize Log Insight](https://www.vmware.com/uk/products/vrealize-log-insight.html), the incoming format is specific to those source systems.  Heimdall also supports two source system agnostic formats; [CloudEvent](https://cloudevents.io/) and native Heimdall.

Before looking at the external systems, it is important to understand the native Heimdall format; that is the format which all other incoming events are converted into prior to rule processing.

You can provide an incoming event payload in this format if you wish, however if you can define the input format yourself then it is likely better to use CloudEvents as that is an industry standard.

# Native Heimdall
A native incoming event has the following json structure.  Internally there are other fields, but they are used for processing purposes and do not form part of an incoming event payload.

```json
{
    "source": "string",
    "type": "string",
    "severity": "string",
    "status": "string",
    "subject": "string",
    "service": "string",
    "system": "string",
    "environment": "string",
    "host": "string",
    "resource": "string",
    "data": {
        "message": "string",
        "value": "string",
        "arbitrary data 3": "string",
        "arbitrary data 4": "string",
        ...
    }
}
```

The data object contains the required `message` and `value` fields and then as many, or as few, key/value fields as needed.  They provide extra information for the alert.

# Alertmanager
The payload format for Alertmanager is documented at https://prometheus.io/docs/alerting/latest/notifications/.

Each payload may contain multiple events; Heimdall will unpack them all and pass each one through for processing.

When parsing the Alertmanager payload, the fields are mapped as follows:

| Alertmanager field  | Heimdall field     |
| ------------------- | ------------------ |
| alertname           | type               |
| instance            | subject            |
| resource            | resource           |
| service             | service            |
| system              | system             |
| environment         | environment        |
| host                | host               |
| severity            | severity           |
| generatorURL        | data.generatorURL  |
| status              | status             |
| startsAt            | time               |
| annotations.summary | data.message       |
| annotations.value   | data.value         |
| labels              | data               |
| annotations         | data               |

Each `CommonLabel`, `GroupLabel` and `Label` is pulled out as a key/value pair and stored under `data`.  Parsing is done in the order shown here, so a `GroupLabel` will override a `CommonLabel` if it has the same name, and a `Label` will override everything else.  The key will be called `label_<label name>`.

For example, the following payload from Alertmanager;

```json
{
    "groupLabels": {
        "label1": "hello",
        "label2": "goodbye",
        "label3": "maybe"
    },
    "commonLabels": {
        "label4": "monday",
        "label2": "tuesday"
    },
    "alerts": [
        {
            "labels": {
                "label5": "january",
                "label3": "february",
                "label2": "march"
            }
        }
    ]
}
```

Will be converted into the following Heimdall structure;

```json
{
    "data": {
        "label_label1": "hello",
        "label_label2": "march",
        "label_label3": "february",
        "label_label4": "monday",
        "label_label5": "january"
    }
}
```

Each `CommonAnnotation` and `Annotation` is pulled out as a key/value pair and stored under `data` in the same way as labels.  An `Annotation` with the same name as a `CommonAnnotation` will win.  The resulting key will be called `annotation_<annotation_name>`.

Within the annotations there should be a field called `summary` and another called `value`.  Although these are optional fields from an Alertmanager perspective, they are required for Heimdall as they populate the `message` and `value` fields which are used for output handlers.

# CloudEvents

**CloudEvents is in beta and is not yet ready for use**

The payload format for CloudEvents is documented at https://cloudevents.io/.  Specifically you are interested in the core spec, https://github.com/cloudevents/spec/blob/v1.0.1/spec.md, and the JSON specific notes, https://github.com/cloudevents/spec/blob/v1.0.1/json-format.md.

CloudEvents is a good choice if you control the development or payload structure of a system.  It is intended to be a standard specification for describing event data in a common way.

When parsing the payload, the fields are mapped as follows:

| CloudEvents field | Heimdall field |
| ----------------- | -------------- |
| source            | source         |
| type              | type           |
| subject           | subject        |
| time              | time           |
| data              | data           |

With CloudEvents, the meat of the event is captured within the `data` field, which is a map of key/value pairs.

# vRealize Log Insight
vRLI can be configured to send to a custom webhook, https://docs.vmware.com/en/vRealize-Log-Insight/8.4/com.vmware.log-insight.administration.doc/GUID-083B7FEC-545A-409F-93EE-009A07D75699.html.  The payload format is not very well documented; the actual event data is in a `messages.fields` array as key/value pairs.  The key is in a field named `name` and the value is in a field named `content`.

Each payload may contain multiple events within the messages array; Heimdall will unpack them all and pass each one through for processing.

Fields map as follows:

| vRLI field                           | Heimdall field |
| ------------------------------------ | -------------- |
| type                                 | type           |
| status                               | status         |
| subject                              | subject        |
| service                              | service        |
| severity                             | severity       |
| data.messages.text                   | data.message   |
| data.recomendation                   | data.value     |
| data.messages.fields.name = hostname | host           |
| data.messages.fields.name = appname  | resource       |
| data.messages.timestamp              | time           |

# Atom
See https://gitlab.com/rokett/atom and https://rokett.gitlab.io/atom/ for Atom.

Fields map as follows:

| Atom field             | Heimdall field              |
| ---------------------- | --------------------------- |
| client_ip              | data.client_ip              |
| created_at             | data.created_at             |
| execution_requested_by | data.execution_requested_by |
| execution_time_ms      | data.execution_time_ms      |
| executor               | data.executor               |
| finished_at            | data.finished_at            |
| id                     | data.job_id                 |
| log                    | data.message                |
| parameters             | data.parameters             |
| requeued               | data.requeued               |
| scope                  | data.scope                  |
| script                 | resource                    |
| script                 | subject                     |
| started_at             | data.started_at             |
| status                 | status and data.value       |
| trace_id               | data.trace_id               |
| updated_at             | data.updated_at             |
| worker_ip              | data.worker_ip              |
| worker_name            | data.worker_name            |
| worker_os              | data.worker_os              |
| worker_server_name     | host                        |
| script and status      | type                        |
| `based on status`      | severity                    |
