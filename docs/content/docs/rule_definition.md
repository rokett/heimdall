---
title: "Rule definition"
weight: 4
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

Rules are defined as JSON, but it is a good idea to manage them under version control and wrap your own CI processes around them, in which case it is much easier to write them as YAML.  For ease of documentation they are presented here as YAML, but can easily be converted to JSON either programmatically or via numberous websites such as https://onlineyamltools.com/convert-yaml-to-json.

There **must** be a default rule called `Default Heimdall rule` to allow for any event to be processed regardless of whether there is an event specific rule for it.

The default rule would look something like the example below, and is well commented to help you understand how to configure a rule.  It is recommended that you leave the comments in every rule file stored under version control; there is no downside to doing so and it makes them easier to understand later.

```yaml
# Friendly name for the alert; if the alert is coming from somewhere like AlertManager, this name should match the alert definition there for ease of management.
# Convention is that this config file is named the same as the friendly name; all lowercase with spaces replaced by _.
name: 'Default Heimdall rule'

# Criteria to match the alert on.  All fields are mandatory.  More specific matches win over less specific matches.
# Each parameter value is checked using Regex, so you have the full power of regex at your disposal.  Ensure you test your regex; you can use https://regex101.com/ for this purpose.
# When doing a regex match for zero or more of a character, ensure you use the lazy quantifier; .*? instead of .* to match any character for example.
# If you don't use the lazy quantifier you will likely find, and this depends somewhat on the payload being matched, that there is catastrophic backtracking and so events will not be matched as you expect.
# Matches are always case-insensitive.
match:
    event_type: '.*?'
    subject: '.*?'
    service: '.*?'
    system: '.*?'
    environment: '.*?'
    host: '.*?'
    source: '.*?'
    resource: '.*?'
    data: # Data fields are optional and do not need to be added to every rule
        field1: 'field 1 value'
        field2: 'field 2 value'

# Boosting a score by an arbitrary value allows for a rule to be defined as more specfic than any other rule with the same number of specified matches.
boost_score: 10

# Link to the runbook for this alert.  All alerts MUST have a corresponding runbook which provides further information and explains how to proceed.
runbook: 'https://wiki/issues/default_heimdall_rule'

# Actions to take when alert fires.
#
# Recurrence is the number of times that the alert has been seen in a defined time period.  The time period is defined by the 'clear_recurrence_after' parameter which deletes the recurrence counter after the time period specified after the last seen event.
# If recurrence is omitted the action will fire every time the alert is seen.
#
# Each action allows for some metadata to be changed.
#   For example an alert may initially just be an information event, but if it has been seen x times you may wish to increase the severity to warning.
#   You can also add a message to the alert providing further information as to why it has been raised, and an indication of what to do next.
#
# Handlers takes an array to allow for multiple outputs.  See https://rokett.gitlab.io/heimdall/docs/output_handlers/ for more information on available handlers and their parameters
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "atom"
                parameters:
                    url: "http://localhost:9999"
                    script: "svc.ps1"
                    parameters: "-Name noddy"
                    scope: "@any"
    -
        recurrence: 3
        handlers:
            -
                action: 'alerta'
                parameters:
                    url: "https://alerta.local.domain"
    -
        recurrence: 8
        handlers:
            -
                action: "alerta"
                parameters:
                    url: "https://alerta.local.domain"
                overrides:
                    severity: critical
                    message: This is really important now

# The time period after the last seen event at which the recurrence counter should be cleared.
# Valid units are (h)ours or (m)inutes.
clear_recurrence_after: 2h
```
