---
title: "Output handlers"
weight: 5
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

# Alerta
Alerta requires an authentication token.  You can pass the token to Heimdall using the `auth_tokens` flag in the form `--auth_tokens "alerta=xxxxxxxxx"`.

Mapping of the internal Heimdall event structure to a format that Alerta understands is as follows.

| Heimdall field | Alerta field  |
| -------------- | ------------- |
| `Heimdall`     | type          |
| subject        | resource      |
| type           | event         |
| environment    | environment   |
| severity       | severity      |
| correlate      | correlate     |
| status         | status        |
| service        | service       |
| data.value     | value         |
| data.message   | text          |
| data           | attributes    |

The following fields are configurable in the rule definition.

| Field                         | Description                     | Required? |
| ----------------------------- | --------------------------------| --------- |
| `parameters -> url`           | Sets the URL for the Alerta API | Yes       |
| `overrides -> severity`       | Overrides the `severity` field  | No        |
| `overrides -> type`           | Overrides the `event` field     | No        |
| `overrides -> status`         | Overrides the `status` field    | No        |
| `overrides -> message`        | Overrides the `text` field      | No        |

```yaml
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "alerta"
                parameters:
                    url: "http://localhost:9999"
                overrides:
                    severity: "critical"
                    type: "Broken website"
                    message: "friendly message in here"
```

# Atom
See Atom docs at https://rokett.gitlab.io/atom/ for more API details.

Atom requires an authentication token.  You can pass the token to Heimdall using the `auth_tokens` flag in the form `--auth_tokens "atom=xxxxxxxxx"`.

The following fields are configurable in the rule definition.

| Field                                            | Description                                                                                   | Required? |
| ------------------------------------------------ | --------------------------------------------------------------------------------------------- | --------- |
| `parameters -> url`                              | Sets the URL for the Atom API                                                                 | Yes       |
| `parameters -> script`                           | Sets the script to run                                                                        | Yes       |
| `parameters -> parameters`                       | Defines any parameters for the script.  To use the event subject, use the keyword `{subject}` | No        |
| `parameters -> scope`                            | Defines the job group scope                                                                   | Yes       |
| `parameters -> executor`                         | Tells Atom which executor to use                                                              | Yes       |
| `parameters -> execution_username `              | Defines the account that Atom should execute the job under.                                   | No        |
| `parameters -> expire_queued_job_after_duration` | Sets how long the job should be allowed to queue for in Atom; defaults to 1 hour              | No        |

```yaml
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "atom"
                parameters:
                    url: "http://localhost:9999"
                    script: "Remove-System.ps1"
                    parameters: "-System {subject} -Prod"
                    scope: "powershell:@any"
                    executor: "powershell"
                    expire_queued_job_after_duration: "3h"
```

# Cosmos
Mapping of the internal Heimdall event structure to a format that Alerta understands is as follows.

| Heimdall field | Cosmos field |
| -------------- | ------------ |
| type           | title        |
| data.message   | description  |
| severity       | severity     |
| source         | source       |
| service        | service      |
| status         | status       |
| system         | system       |
| environment    | environment  |
| host           | host         |
| resource       | resource     |
| data.value     | value        |
| `rule runbook` | runbook      |
| data           | fields       |

The following fields are configurable in the rule definition.

| Field                         | Description                       | Required? |
| ----------------------------- | --------------------------------- | --------- |
| `parameters -> url `          | Sets the URL for the Cosmos API   | Yes       |
| `overrides -> severity`       | Overrides the `severity` field    | No        |
| `overrides -> type`           | Overrides the `title` field       | No        |
| `overrides -> message`        | Overrides the `description` field | No        |

```yaml
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "cosmos"
                parameters:
                    url: "http://localhost:9999"
                overrides:
                    severity: "critical"
                    type: "Broken website"
                    message: "friendly message in here"
```

# Discard
Events are discarded when sent to this handler.  Handy for known events that you don't care about, but are unable to get rid of at the source.

```yaml
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "discard"
```

# Email
The `From` address of the email will be as defined by the application flag or within the rule, however the name will always be `Heimdall`.

To use the email handler, the application can either be started with the relevant flags, or they can be included in the rule definition.  It would be best practice to define an application level config using the flags, and then override as needed within specific rules.

| Flag                 | Description                                                   | Default Value  | Required? |
| -------------------- | ------------------------------------------------------------- | -------------- | --------- |
| `email_host`         | The SMTP host used to send email if the email handler is used |                | No        |
| `email_port`         | The TCP port to use to access the SMTP host                   | 25             | No        |
| `email_from_address` | From address for email sent from Heimdall                     |                | No        |

The following fields are configurable in the rule definition.

| Field                       | Description                                        | Required? |
| --------------------------- | -------------------------------------------------- | --------- |
| `parameters -> to`          | Comma separated list of email addresses to send to | Yes       |
| `parameters -> subject`     | Subject for the email                              | Yes       |
| `parameters -> message`     | Message body for the email                         | Yes       |
| `overrides -> from_address` | Overrides the application level FROM address       | No        |
| `overrides -> smtp_host`    | Sets the email host to send email via              | No        |
| `overrides -> smtp_port`    | Defines the SMTP port                              | No        |

```yaml
actions:
    -
        recurrence: 1
        handlers:
            -
                action: "email"
                parameters:
                    to: "me@me.com"
                    subject: "System down"
                    message: "The primary system is down.  Check https://wiki.me.com/system runbook for what to do next"
                overrides:
                    from_address: "heimdall@me.com"
                    smtp_host: "smtp.me.com"
                    smtp_port: 25
```
