---
title: "Architecture"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

There isn't much to Heimdall; it really just consists of the application itself and a database (Only MS SQL Server at the moment) to store event information in.

Because it is the core of your event handling service, it makes sense to ensure it is resilient.  It is recommended to put Heimdall behind a load balancer, and to cluster the database.  The load balancer also provides a convenient place to terminate SSL connections.

![Architecture](../architecture.png "Architecture")

## REST API
The API provides an interface for managing rules.

## NATS
The [NATS](https://nats.io) message queue is the conduit for rule updates and deletions between servers.  Changes to jobs are written to the database and published to NATS, which the servers then pickup in order to update their own in-memory stores.

# Database
The database stores active events, a full event history, and rule definitions.

# Ports
A number of different ports are required to be open for Heimdall to work properly.

![Ports diagram](../ports.png "Heimdall ports")

| Port  | Purpose                                                                    |
| ----- | -------------------------------------------------------------------------- |
| 10002 | Public HTTP API access                                                     |
| 4248  | TCP based intra-cluster comms between server nodes for NATS                |
| 4222  | TCP based comms between workers/servers and the servers for NATS           |

# Clustering the server
In order to provide HA, we need to ensure the server is clustered in order for the nodes to know about each other.  An HA setup requires a minimum of two nodes.

To cluster the server we make use of the `nats-cluster-routes` flag.

```shell
# Server 1
heimdalld.exe --nats-cluster-routes "nats://server1:4248,nats://server2:4248,nats://server3:4248" --deployment.environment "production"

# Server 2
heimdalld.exe --nats-cluster-routes "nats://server1:4248,nats://server2:4248,nats://server3:4248" --deployment.environment "production"

# Server 3
heimdalld.exe --nats-cluster-routes "nats://server1:4248,nats://server2:4248,nats://server3:4248" --deployment.environment "production"
```

These three servers will join the cluster and will see all NATS.  You can add more nodes if you want to; there is no need for `--nats-cluster-routes` to reference every node in the cluster because when new nodes join they will learn about other cluster nodes.
