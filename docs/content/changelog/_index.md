# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.18.0] - 2024/01/12
### Changed
- #62 Refactored to allow the HTTP request timeout to be configured via a flag. The default remains at 5 seconds, but if needed it can be changed without releasing a new version and hardcoding the value.
- #63 To ensure that incoming event requests are closed as early as possible, without waiting for the event to be processed and sent on, the processing of individual events is now done in a goroutine.

## [0.17.1] - 2024/01/08
### Added
- Logging rules and match strings in traces to allow for easier troubleshooting.

### Changed
- #61 Ensure that incoming requests are closed.

## [0.17.0] - 2023/12/29
### Changed
- #59 Moved handler overrides into their own section instead of being dealt with as parameters of the handler.  See https://rokett.gitlab.io/heimdall/docs/output_handlers/ for how to define the overrides.

## [0.16.1] - 2023/12/28
### Fixed
- #58 Allowing for rules to have their score boosted, by adding the `boost_score` parameter to the rule definition, resolves a problem where two matching rules may have the same score even though one is technically more specific than the other.

## [0.16.0] - 2023/11/14
### Added
- #56 Added OTEL trace ID to Alert and Cosmos outputs payloads.

### Changed
- #57 Tweaked rule matching to remove potential problems in matching like new lines, and to explain in the documentation that the regex used should use the lazy rather than greedy quantifier.

## [0.15.0] - 2023/11/13
### Added
- #55 Ensuring `runbook` field in the rule appears in Alerta.

## [0.14.0] - 2023/11/06
### Changed
- #54 Added `host.name` resource to traces to identify the host that Heimdall is running on.  Also moved `deployment.environment` from a span attribute to be a resource.

## [0.13.0] - 2023/10/27
### Fixed
- #45 Refactored event fingerprinting to be source specific; this allows us to easly customise the fields used to generate the fingerprint, avoiding some events being seen as unique when they are not.

  For example, an incoming event from AlertManager could come from any AlertManager server.  The event includes a `generatorURL` field which identifies the sending instance.  This field was previously included in the event fingerprint and therefore the same even, originating from more than one AlertManager instance, would be seen as unique when it is not.

### Added
- #53 New option to test an event.  Adding `test` to the querystring when POSTing an event, `POST https://heimdall.xxx/event?source=alertmanager&test`, will process the event but stop short of actually sending it to the defined output.  A log message will be printed to identify it as a test with the relevant payload.

### Changed
- #39 Removed logging for HTTP requests in favour of tracing.  Tracing provides more contextual information and allows one to visualise the path a request takes much easier.  Use any tracing platform which supports OpenTelemetry.
- #39 Removed option to enable debugging.  There is no longer any need as there is no additional debugging info; as much information as possible is added to the traces as a request flows through the system.

## [0.12.0] - 2023/09/21
### Added
- #52 New option to override the `status` field for the Alerta handler in the rule definition.  Add a `status_override` field to the `parameters` array to fix a specific status.

## [0.11.0] - 2023/09/11
### Fixed
- #51 Resolved issue with NATS client connection instantly failing on startup by setting the `RetryOnFailedConnect` option for NATS; the client will retry the intial connection based on the `MaxReconnects` option.

### Changed
- #34 Replaced Gorilla toolkit in favour of Chi.  Gorilla was unsupported for a while, although may be back in active maintenance now.
- #47 Mapped `script` from Atom payload to `subject` to ensure events can be forwarded to Alerta.
- #48 Updated all dependencies and now require Go version 1.21.x to build Heimdall.
- #50 Removed `github.com/pkg/errors` library in favour of stdlib.

### Added
- #49 Added support for `expire_queued_job_after_duration` for the Atom handler.  This parameter defines how long a job should be allowed to queue for in Atom before timing out.  Heimdall default is 1 hour.

## [0.10.0] - 2023/01/12
### Added
- #43 Now supporting `execution_username` for the Atom handler.  This parameter tells Atom to run a job as a specific user.

## [0.9.0] - 2023/01/12
### Added
- #41 Logging response status code for all HTTP handlers.
- #42 Additional logging of handler details; `url` for HTTP handlers and `to_address` for the email handler.

## [0.8.0] - 2022/11/13
### Added
- #37 Improving traceability by adding a payload trace ID.  This is useful when the payload contains multiple events, such as from Alertmanager.  Previously each event would have a trace ID, but it wasn't easy to match this back to the original payload which wasn't traced.

  Added the `payload_trace_id` field to logging to accommodate the new trace.

## [0.7.0] - 2022/09/11
### Changed
- #32 Replaced `https://github.com/denisenkom/go-mssqldb` with new official package, `https://github.com/microsoft/go-mssqldb`.
- #33 Reconfigured migrations to strip line breaks in order to ensure that the resulting hash is the same on Windows and Linux.  This allows for porting between the two, which may be unlikely, but without this change running the migrations would fail.

  **Important:** To support this change you must delete all records from the migrations table.  This will force migrations to run again, which are idempotent from a schema perspective.  No other changes are necessary.
- #35 Ensure that container runs as a non-root user with UID 10000 and GID 10001.

## [0.6.0] - 2022/05/25
### Added
- `event_data` field has been added to log entries where applicable.
- #24 Events can now be matched against data fields, in addition to fixed fields.  See documentation for details.
- #26 Build container image for Heimdall to allow running as a container.

### Changed
- #29 Accept `resource` field from Alertmanager; map to Heimdall `resource` field.

## [0.5.0] - 2022/03/09
### Added
- #27 New handler named `discard` to send events to a black hole.  Handy for known events that you don't care about, but are unable to get rid of at the source.

### Changed
- #25 Updated output handler documentation to show how internal Heimdall event fields map to Alerta and Cosmos output formats.
- #28 Updated dependencies.

## [0.4.0] - 2022/01/25
### Added
- #23 New endpoint, `GET /rules`, to return a list of rule names.  Useful for when you are wanting to programmatically determine which rules should be deleted if you store them in a VCS.  Get a list from Heimdall, compare them with what's in your VCS, and then remove any which are not in the VCS.

## [0.3.1] - 2022/01/17
### Fixed
- #22 Alerta expects an incoming event with a status of 'firing', predominantly coming from Alertmanager at the current time, to be set to 'open' to ensure it is visible in Alerta.

## [0.3.0] - 2022/01/05
### Changed
- #17 Removed duplicate `message` and `value` fields from Alerta payload.

### Added
- #15 Incoming events from Alertmanager may have a correlate field which can be used to correlate events and only show the most significant.  We now send this field on to Alerta.
- #16 Added new `executor` parameter to the Atom handler to match that required by the Atom API.

## [0.2.0] - 2021/12/08
### Added
- #3 When processing events, ensure the source is added to any log entries.
- #5 New endpoint, `resetEvent`, to allow other systems to feed back in to reset the event recurrence and status.  For example, resolving an alert at Alerta or Cosmos should mean that the event status in Heimdall is set to resolved and the recurrent count is zeroed.
- #6 Added handler for Cosmos (https://gitlab.com/sparkblaze/cosmos).
- #7 Added processor for events coming from VMware vRealize Log Insight.
- #13 New endpoint created to validate a rule.  It returns a `200` if the rule is valid and a `400` if it is not. This endpoint is really useful for CI as a rule can be validated before it is merged and promoted to production.

### Changed
- #8 Reconfigured the module path from `heimdall` to `rokett.me/heimdall` to better match Go standards.
- #9 Updated backend dependencies.
- #10 Added stactrace to log entries of error level and above.
- #12 Rules are no longer stored as YAML files in the filesystem.  Operators now use `PUT /rules` to upsert rules, and `DELETE /rules` to delete them; both as JSON.

## [0.1.1] - 2021/06/29
### Fixed
- #1 Emails are now sent to multiple addresses specified in the rule definition.

## [0.1.0] - 2021/06/27
- Initial test release
