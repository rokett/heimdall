package heimdall

import (
	"embed"
)

// Migrations represents the migrations folder
//go:embed migrations
var Migrations embed.FS
