package fingerprint

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
)

func Calculate(fields []string) (fingerprint string, err error) {
	bs, err := json.Marshal(fields)
	if err != nil {
		return fingerprint, fmt.Errorf("unable to marshal fields to calculate fingerprint: %w", err)
	}

	hash := sha256.Sum256(bs)
	fingerprint = hex.EncodeToString(hash[:])

	return fingerprint, nil
}
