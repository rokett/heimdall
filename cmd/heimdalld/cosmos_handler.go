package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type cosmosHandler struct {
	URL         string            `json:"-"`
	Title       string            `json:"title"`
	Description string            `json:"description"`
	Severity    string            `json:"severity"`
	Source      string            `json:"source"`
	Status      string            `json:"status"`
	Service     string            `json:"service"`
	System      string            `json:"system"`
	Environment string            `json:"environmnet"`
	Host        string            `json:"host"`
	Resource    string            `json:"resource"`
	Value       string            `json:"value"`
	Runbook     string            `json:"runbook"`
	Fields      map[string]string `json:"fields"`
}

func (h cosmosHandler) run(ctx context.Context, config *config, isTest bool) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"cosmosHandler.run",
	)
	defer span.End()

	payload, err := json.Marshal(h)
	if err != nil {
		// Add `handler` field and remove references to Cosmos.  Should allow us to use largely the same code for Alerta and Atom.
		span.SetStatus(codes.Error, "converting payload to JSON")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	span.SetAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
		attribute.Bool("handler.isTest", isTest),
	)

	if isTest {
		return
	}

	client := &http.Client{
		Timeout: time.Second * 20,
	}

	req, err := http.NewRequest("POST", h.URL, bytes.NewBuffer(payload))
	if err != nil {
		span.SetStatus(codes.Error, "creating HTTP request")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	req.Header.Add("Content-Type", "application/json")

	span.AddEvent("sending payload to Cosmos", trace.WithAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
	))

	resp, err := client.Do(req)
	if err != nil {
		span.SetStatus(codes.Error, "sending event")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		span.SetAttributes(attribute.Int("handler.response_status_code", resp.StatusCode))
		span.SetStatus(codes.Error, "reading response body")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	span.SetAttributes(
		attribute.String("handler.response", string(body)),
		attribute.Int("handler.response_status_code", resp.StatusCode),
	)

	switch resp.StatusCode {
	case 200, 201:
		span.AddEvent("payload accepted")
	default:
		span.SetStatus(codes.Error, "payload rejected")
	}
}
