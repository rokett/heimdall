package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) validateRule(w http.ResponseWriter, r *http.Request) {
	_, span := app.tracer.Start(
		r.Context(),
		"validateRule",
	)
	defer span.End()

	defer r.Body.Close()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		span.SetStatus(codes.Error, "reading HTTP request body")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to read HTTP request body; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))

		return
	}
	span.SetAttributes(attribute.String("http.request.body", string(body)))

	var rule rule

	// We decode the incoming JSON formatted rule to ensure it is valid
	err = decodeJSON(body, &rule)
	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			span.SetStatus(codes.Error, mr.msg)
			span.RecordError(err, trace.WithStackTrace(true))

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(mr.msg))
		} else {
			span.SetStatus(codes.Error, "decoding rule")
			span.RecordError(err, trace.WithStackTrace(true))

			msg := fmt.Sprintf("unable to decode rule; %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(msg))
		}
		return
	}

	err = isRuleValid(rule)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		span.RecordError(err, trace.WithStackTrace(true))

		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	span.AddEvent("rule is valid")

	w.WriteHeader(http.StatusOK)
}
