package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"rokett.me/heimdall/internal/uuid"
)

func (app *application) processEvent(ctx context.Context, event Event, isTest bool) {
	ctx, span := app.tracer.Start(
		ctx,
		"processEvent",
	)
	defer span.End()

	eventTxId, err := uuid.NewV4()
	if err != nil {
		span.SetStatus(codes.Error, "generating event transaction ID")
		span.RecordError(err, trace.WithStackTrace(true))
	}

	span.SetAttributes(
		attribute.String("event.trace_id", eventTxId.String()),
		attribute.String("event.type", event.Type),
		attribute.String("event.subject", event.Subject),
		attribute.String("event.service", event.Service),
		attribute.String("event.system", event.System),
		attribute.String("event.environment", event.Environment),
		attribute.String("event.host", event.Host),
		attribute.String("event.source", event.Source),
		attribute.String("event.resource", event.Resource),
		attribute.StringSlice("event.correlate", event.Correlate),
		attribute.StringSlice("event.fingerprintRaw", event.FingerprintRaw),
		attribute.String("event.fingerprint", event.Fingerprint),
		attribute.String("config.Rules", app.config.RulesJson),
		attribute.String("config.RuleMatches", app.config.RuleMatchesJson),
	)

	matchString := fmt.Sprintf("%s:%s:%s:%s:%s:%s:%s:%s", event.Type, event.Subject, event.Service, event.System, event.Environment, event.Host, event.Source, event.Resource)
	for k, v := range event.Data {
		s := strings.ReplaceAll(v, "\r\n", "")
		s = strings.ReplaceAll(s, "\n", "")
		matchString = fmt.Sprintf("%s:%s-%s", matchString, k, s)
	}
	matchString = fmt.Sprintf("%s:", matchString)
	span.SetAttributes(attribute.String("event.matchString", matchString))

	var found bool

	var eventRule rule

	// We need to loop through all possible matching rules and find the one with the highest specificity score; that will be the rule we want to use.
	var score int

	for k, v := range app.config.RuleMatches {
		span.SetAttributes(
			attribute.String("rule.name", v),
			attribute.String("rule.match", k),
			attribute.Int("rule.score", score),
		)

		re := regexp.MustCompile(k)
		if re.MatchString(matchString) {
			possibleRule, err := getRule(v, app.config.Rules)
			if err != nil {
				span.SetStatus(codes.Error, "retrieving rule definition based on match")
				span.RecordError(err, trace.WithStackTrace(true))

				continue
			}

			// Score starts off at 0 and when we find a possible rule we get it's specificity score.
			// Subsequent loops check the score against any other matching rules, and the rule with the highest score will come out as the victor.
			// That means we end up with a matching rule with the highest specificity which allows us to target rules at, for example, specific environments and/or hosts.
			if score < possibleRule.Score {
				score = possibleRule.Score

				eventRule = possibleRule
				found = true

				span.AddEvent("found potential matching rule", trace.WithAttributes(
					attribute.String("rule.name", v),
					attribute.String("rule.match", k),
					attribute.String("event.matchString", matchString),
					attribute.Int("rule.score", score),
				))
			}
		}
	}

	if !found {
		span.SetAttributes(
			attribute.String("rule.name", "Default Heimdall rule"),
			attribute.String("rule.match", ""),
		)

		span.AddEvent("unable to find matching rule; sending to default route")

		eventRule, err = getRule("Default Heimdall rule", app.config.Rules)
		if err != nil {
			span.SetStatus(codes.Error, "unable to retrieve default rule")

			return
		}
	}

	var dbEvent Event

	err = app.db.Get(&dbEvent, "SELECT id, status, lifetime_count, recurrence_count, updated_at FROM events WHERE checksum = @p1", event.Fingerprint)
	if err != nil && err != sql.ErrNoRows {
		span.SetStatus(codes.Error, "finding event in DB with matching event.Fingerprint")
		span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("SELECT id, status, lifetime_count, recurrence_count, updated_at FROM events WHERE checksum = '%s'", event.Fingerprint)))
		span.RecordError(err, trace.WithStackTrace(true))

		return
	}

	// TODO Insert/Update arbitrary data fields
	// TODO Insert timestamp

	now := time.Now().UTC().Format(time.RFC3339)

	jsonEventData, err := json.Marshal(event.Data)
	if err != nil {
		span.SetStatus(codes.Error, "encoding event data to JSON")
		span.RecordError(err, trace.WithStackTrace(true))

		return
	}

	switch dbEvent.ID {
	case 0:
		event.RecurrenceCount = 1
		event.LifetimeCount = 1

		err = app.db.Get(&dbEvent.ID, "INSERT INTO events (source, type, status, lifetime_count, recurrence_count, subject, service, system, environment, host, resource, checksum, correlate, created_at, updated_at, data) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16);SELECT id = convert(bigint, SCOPE_IDENTITY());", event.Source, event.Type, "open", event.LifetimeCount, event.RecurrenceCount, event.Subject, event.Service, event.System, event.Environment, event.Host, event.Resource, event.Fingerprint, strings.Join(event.Correlate, ","), now, now, string(jsonEventData))
		if err != nil {
			span.SetStatus(codes.Error, "adding new event")
			span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("INSERT INTO events (source, type, status, lifetime_count, recurrence_count, subject, service, system, environment, host, resource, checksum, correlate, created_at, updated_at, data) VALUES ('%s', '%s', '%s', %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');SELECT id = convert(bigint, SCOPE_IDENTITY());", event.Source, event.Type, "open", event.LifetimeCount, event.RecurrenceCount, event.Subject, event.Service, event.System, event.Environment, event.Host, event.Resource, event.Fingerprint, strings.Join(event.Correlate, ","), now, now, string(jsonEventData))))
			span.RecordError(err, trace.WithStackTrace(true))

			return
		}

		span.SetAttributes(
			attribute.Int64("event.id", dbEvent.ID),
			attribute.Int64("event.lifetimeCount", event.LifetimeCount),
			attribute.Int64("event.recurrentCount", event.RecurrenceCount),
			attribute.String("event.status", "open"),
		)
	default:
		// If the event is resolved we don't want to increment the counts but we do want to ensure they don't get reset to 0, so we set them to whatever the DB record currently says.
		if event.Status == "resolved" {
			event.RecurrenceCount = dbEvent.RecurrenceCount
			event.LifetimeCount = dbEvent.LifetimeCount
		} else {
			// Initially we set the event recurrence count to increment whatever the DB says, but we need to check whether that should be cleared.
			// This is done by checking whether the last updated date (in other words the last time this event was seen) is older than the 'clear_recurrence_after' timeframe set in the rule definition.
			event.RecurrenceCount = dbEvent.RecurrenceCount + 1
			if time.Since(dbEvent.UpdatedAt) > eventRule.ClearRecurrenceAfterDuration {
				event.RecurrenceCount = 1
			}

			event.LifetimeCount = dbEvent.LifetimeCount + 1

			event.Status = "open"
		}

		span.SetAttributes(
			attribute.Int64("event.id", dbEvent.ID),
			attribute.Int64("event.lifetimeCount", event.LifetimeCount),
			attribute.Int64("event.recurrentCount", event.RecurrenceCount),
			attribute.String("event.status", event.Status),
		)

		_, err := app.db.Exec("UPDATE events SET status = @p1, lifetime_count = @p2, recurrence_count = @p3, updated_at = @p4 WHERE id = @p5", event.Status, event.LifetimeCount, event.RecurrenceCount, now, dbEvent.ID)
		if err != nil {
			span.SetStatus(codes.Error, "updating existing event")
			span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("UPDATE events SET status = '%s', lifetime_count = %d, recurrence_count = %d, updated_at = '%s' WHERE id = %d", event.Status, event.LifetimeCount, event.RecurrenceCount, now, dbEvent.ID)))
			span.RecordError(err, trace.WithStackTrace(true))

			return
		}
	}

	_, err = app.db.Exec("INSERT INTO event_history (source, type, status, subject, service, system, environment, host, resource, event_id, correlate, created_at, updated_at, data) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14);", event.Source, event.Type, event.Status, event.Subject, event.Service, event.System, event.Environment, event.Host, event.Resource, dbEvent.ID, strings.Join(event.Correlate, ","), event.Time, event.Time, string(jsonEventData))
	if err != nil {
		span.SetStatus(codes.Error, "adding event to history")
		span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("INSERT INTO event_history (source, type, status, subject, service, system, environment, host, resource, event_id, correlate, created_at, updated_at, data) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s');", event.Source, event.Type, event.Status, event.Subject, event.Service, event.System, event.Environment, event.Host, event.Resource, dbEvent.ID, strings.Join(event.Correlate, ","), event.Time, event.Time, string(jsonEventData))))
		span.RecordError(err, trace.WithStackTrace(true))
	}

	var act action
	for _, action := range eventRule.Actions {
		if action.Recurrence <= event.RecurrenceCount {
			act = action
		}
	}

	for _, action := range act.Handlers {
		switch strings.ToLower(action.Action) {
		case "atom":
			span.AddEvent("setting event action handler to Atom", trace.WithAttributes(
				attribute.String("handler.url", action.Parameters["url"]),
				attribute.String("handler.script", action.Parameters["script"]),
			))

			var re = regexp.MustCompile(`(?m){subject}`)
			params := re.ReplaceAllString(action.Parameters["parameters"], event.Subject)

			expireQueuedJobAfterDuration := "1h"
			if action.Parameters["expired_queued_job_after_duration"] != "" {
				expireQueuedJobAfterDuration = action.Parameters["expired_queued_job_after_duration"]
			}

			atomHandler{
				URL:                          action.Parameters["url"],
				Script:                       action.Parameters["script"],
				Parameters:                   params,
				Scope:                        action.Parameters["scope"],
				Executor:                     action.Parameters["executor"],
				ExecutionUsername:            action.Parameters["execution_username"],
				ExpireQueuedJobAfterDuration: expireQueuedJobAfterDuration,
			}.run(ctx, &app.config, isTest)
		case "alerta":
			span.AddEvent("setting event action handler to Alerta", trace.WithAttributes(
				attribute.String("handler.url", action.Parameters["url"]),
			))

			// Alerta doesn't recognise `resolved`; it needs to be `closed`
			if event.Status == "resolved" {
				event.Status = "closed"
			}

			// Alerta expects a firing event to be open
			if event.Status == "firing" {
				event.Status = "open"
			}

			if action.Overrides.Status != "" {
				event.Status = action.Overrides.Status
			}

			severity := event.Severity
			if action.Overrides.Severity != "" {
				severity = action.Overrides.Severity
			}

			eventType := event.Type
			if action.Overrides.Type != "" {
				eventType = action.Overrides.Type
			}

			text := event.Data["message"]
			if action.Overrides.Message != "" {
				text = action.Overrides.Message
			}
			delete(event.Data, "message")

			value := event.Data["value"]
			delete(event.Data, "value")

			event.Data["runbook"] = eventRule.Runbook
			event.Data["trace_id"] = span.SpanContext().TraceID().String()

			alertaHandler{
				URL:         action.Parameters["url"],
				Resource:    event.Subject,
				Event:       eventType,
				Environment: event.Environment,
				Status:      event.Status,
				Service:     []string{event.Service},
				Text:        text,
				Value:       value,
				Severity:    severity,
				Correlate:   event.Correlate,
				Attributes:  event.Data,
			}.run(ctx, &app.config, isTest)
		case "cosmos":
			span.AddEvent("setting event action handler to Cosmos", trace.WithAttributes(
				attribute.String("handler.url", action.Parameters["url"]),
			))

			eventType := event.Type
			if action.Overrides.Type != "" {
				eventType = action.Overrides.Type
			}

			description := event.Data["message"]
			if action.Overrides.Message != "" {
				description = action.Overrides.Message
			}

			severity := event.Severity
			if action.Overrides.Severity != "" {
				severity = action.Overrides.Severity
			}

			event.Data["trace_id"] = span.SpanContext().TraceID().String()

			cosmosHandler{
				URL:         action.Parameters["url"],
				Title:       eventType,
				Description: description,
				Severity:    severity,
				Source:      event.Source,
				Status:      event.Status,
				Service:     event.Service,
				System:      event.System,
				Environment: event.Environment,
				Host:        event.Host,
				Resource:    event.Resource,
				Value:       event.Data["value"],
				Runbook:     eventRule.Runbook,
				Fields:      event.Data,
			}.run(ctx, &app.config, isTest)
		case "discard":
			discardHandler{}.run(ctx, isTest)
			return
		case "email":
			span.AddEvent("setting event action handler to email", trace.WithAttributes(
				attribute.StringSlice("handler.email_to", strings.Split(action.Parameters["to"], ",")),
			))

			from := app.config.Email.From
			if action.Overrides.FromAddress != "" {
				from = action.Overrides.FromAddress
			}

			host := app.config.Email.Host
			if action.Overrides.SMTPHost != "" {
				from = action.Overrides.SMTPHost
			}

			port := app.config.Email.Port
			if action.Overrides.SMTPPort != 0 {
				port = action.Overrides.SMTPPort
			}

			emailHandler{
				SMTPHost: host,
				SMTPPort: port,
				To:       strings.Split(action.Parameters["to"], ","),
				From:     from,
				Subject:  action.Parameters["subject"],
				Body:     action.Parameters["message"],
			}.run(ctx, &app.config, isTest)
		default:
			span.SetStatus(codes.Error, "event action not recognised")
		}
	}
}
