package main

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"go.opentelemetry.io/otel"
	"rokett.me/heimdall/internal/fingerprint"
)

// AtomPayload represents the expected JSON structure received from Atom (https://gitlab.com/rokett/atom)
type AtomPayload struct {
	Id                   int64
	ClientIp             string `json:"client_ip"`
	ExecutionRequestedBy string `json:"execution_requested_by"`
	ExecutionTimeMs      int64  `json:"execution_time_ms"`
	Executor             string
	Parameters           string
	Requeued             int64
	Scope                string
	Script               string
	Status               string
	TraceId              string `json:"trace_id"`
	WorkerIp             string `json:"worker_ip"`
	WorkerName           string `json:"worker_name"`
	WorkerOs             string `json:"worker_os"`
	WorkerServerName     string `json:"worker_server_name"`
	Log                  string
	StartedAt            time.Time `json:"started_at"`
	FinishedAt           time.Time `json:"finished_at"`
	CreatedAt            time.Time `json:"created_at"`
	UpdatedAt            time.Time `json:"updated_at"`
}

func processAtomPayload(ctx context.Context, srcEvent []byte) (events []Event, err error) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"processAtomPayload",
	)
	defer span.End()

	var event AtomPayload

	err = json.Unmarshal(srcEvent, &event)
	if err != nil {
		return events, fmt.Errorf("unable to decode Atom event: %w", err)
	}

	var eventSeverity string
	switch event.Status {
	case "COMPLETED":
		eventSeverity = "information"
	case "COMPLETED_WITH_WARNINGS":
		eventSeverity = "warning"
	case "ERROR", "ERROR_STARTING", "UNAUTHORISED_SCRIPT":
		eventSeverity = "critical"
	default:
		eventSeverity = "warning"
	}

	evData := map[string]string{
		"client_ip":              event.ClientIp,
		"created_at":             event.CreatedAt.String(),
		"execution_requested_by": event.ExecutionRequestedBy,
		"execution_time_ms":      strconv.Itoa(int(event.ExecutionTimeMs)),
		"executor":               event.Executor,
		"finished_at":            event.FinishedAt.String(),
		"job_id":                 strconv.Itoa(int(event.Id)),
		"log":                    event.Log,
		"message":                event.Log,
		"parameters":             event.Parameters,
		"requeued":               strconv.Itoa(int(event.Requeued)),
		"scope":                  event.Scope,
		"started_at":             event.StartedAt.String(),
		"value":                  event.Status,
		"trace_id":               event.TraceId,
		"updated_at":             event.UpdatedAt.String(),
		"worker_ip":              event.WorkerIp,
		"worker_name":            event.WorkerName,
		"worker_os":              event.WorkerOs,
	}

	// Now we need to build a fingerprint to help us identify duplicate events
	fingerprintRaw := []string{
		event.Script,
		event.WorkerServerName,
		"atom",
		"atom",
		"",
		fmt.Sprintf("%s - %s", event.Script, event.Status),
		event.Script,
		evData["parameters"],
	}

	fingerprint, err := fingerprint.Calculate(fingerprintRaw)
	if err != nil {
		return events, fmt.Errorf("unable to calculate fingerprint: %w", err)
	}

	events = append(events, Event{
		Source:         "atom",
		System:         "atom",
		Service:        "atom",
		Resource:       event.Script,
		Subject:        event.Script,
		Status:         event.Status,
		Host:           event.WorkerServerName,
		Type:           fmt.Sprintf("%s - %s", event.Script, event.Status),
		Severity:       eventSeverity,
		Data:           evData,
		FingerprintRaw: fingerprintRaw,
		Fingerprint:    fingerprint,
	})

	return events, nil
}
