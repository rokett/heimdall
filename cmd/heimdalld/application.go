package main

import (
	"github.com/jmoiron/sqlx"
	"github.com/nats-io/nats.go"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type application struct {
	config config
	db     *sqlx.DB
	ec     *nats.EncodedConn
	logger *zap.SugaredLogger
	tracer trace.Tracer
}
