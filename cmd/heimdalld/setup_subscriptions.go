package main

import (
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

func setupSubscriptions(config *config, ec *nats.EncodedConn, logger *zap.SugaredLogger, db *sqlx.DB) {
	_, err := ec.Subscribe("rule_updated", func(id int64) {
		r, err := getSingleRule(id, db)
		if err != nil {
			logger.Errorw("unable to retrieve rule",
				"rule_id", id,
				"error", err,
			)
			return
		}

		parseRuleDefinition(r, logger, config)
	})
	if err != nil {
		logger.Fatalw("unable to setup subscription",
			"error", err,
			"nats_subscription", "rule_update",
		)
	}

	_, err = ec.Subscribe("rule_removed", func(name string) {
		for k, r := range config.RuleMatches {
			if strings.EqualFold(r, name) {
				delete(config.RuleMatches, k)
				break
			}
		}

		for k, r := range config.Rules {
			if strings.EqualFold(r.Name, name) {
				config.Rules = append(config.Rules[:k], config.Rules[k+1:]...)
				break
			}
		}
	})
	if err != nil {
		logger.Fatalw("unable to setup subscription",
			"error", err,
			"nats_subscription", "rule_removed",
		)
	}
}
