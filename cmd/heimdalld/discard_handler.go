package main

import (
	"context"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
)

type discardHandler struct{}

func (h discardHandler) run(ctx context.Context, isTest bool) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"discardHandler.run",
	)
	defer span.End()

	span.SetAttributes(attribute.Bool("handler.isTest", isTest))
}
