package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"go.opentelemetry.io/otel"
	"rokett.me/heimdall/internal/fingerprint"
)

// CloudEvent represents an incoming event in CloudEvents format.
type CloudEvent struct {
	ID              string            `json:"id"`
	Source          string            `json:"source"`
	SpecVersion     string            `json:"specversion"`
	Type            string            `json:"type"`
	DataContentType string            `json:"datacontenttype"`
	Subject         string            `json:"subject"`
	Time            time.Time         `json:"time"`
	Data            map[string]string `json:"data"`
}

func processCloudEventsPayload(ctx context.Context, srcEvent []byte) (events []Event, err error) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"processCloudEventsPayload",
	)
	defer span.End()

	var event CloudEvent

	err = json.Unmarshal(srcEvent, &event)
	if err != nil {
		return events, fmt.Errorf("unable to decode CloudEvents event: %w", err)
	}

	// Now we need to build a fingerprint to help us identify duplicate events
	fingerprintRaw := []string{
		"",
		"",
		"",
		"",
		"",
		event.Type,
		event.Subject,
	}

	fingerprint, err := fingerprint.Calculate(fingerprintRaw)
	if err != nil {
		return events, fmt.Errorf("unable to calculate fingerprint: %w", err)
	}

	events = append(events, Event{
		Source:         event.Source,
		Type:           event.Type,
		Subject:        event.Subject,
		Time:           event.Time,
		Data:           event.Data,
		FingerprintRaw: fingerprintRaw,
		Fingerprint:    fingerprint,
	})

	return events, nil
}
