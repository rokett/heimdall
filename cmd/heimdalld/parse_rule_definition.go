package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"go.uber.org/zap"
)

func parseRuleDefinition(ruleDef ruleDefinition, logger *zap.SugaredLogger, config *config) {
	var rule rule

	// We decode the incoming JSON formatted rule to ensure it is valid
	err := decodeJSON([]byte(ruleDef.Definition), &rule)
	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			logger.Errorw("unable to decode rule definition",
				"error", mr.msg,
				"rule_id", ruleDef.ID,
			)
		} else {
			logger.Errorw("unable to decode rule definition",
				"error", err,
				"rule_id", ruleDef.ID,
			)
		}
		return
	}

	rule.ClearRecurrenceAfterDuration, err = time.ParseDuration(rule.ClearRecurrenceAfter)
	if err != nil {
		logger.Errorw("unable to parse 'clear_recurrence_after' parameter",
			"error", err,
			"rule_id", ruleDef.ID,
		)
		return
	}

	var score int

	if rule.Match.EventType != ".*?" {
		score++
	}

	if rule.Match.Subject != ".*?" {
		score++
	}

	if rule.Match.Service != ".*?" {
		score++
	}

	if rule.Match.System != ".*?" {
		score++
	}

	if rule.Match.Environment != ".*?" {
		score++
	}

	if rule.Match.Host != ".*?" {
		score++
	}

	if rule.Match.Source != ".*?" {
		score++
	}

	if rule.Match.Resource != ".*?" {
		score++
	}

	score = score + len(rule.Match.Data)

	if rule.BoostScore != 0 {
		score = score + rule.BoostScore
	}

	rule.Score = score

	// The rule may be a modification of an existing rule.
	// So we need to search the slice to see if there is an existing rule, and if there is we modify its contents.
	newRule := true
	for k, r := range config.Rules {
		if strings.EqualFold(r.Name, rule.Name) {
			config.Rules[k] = rule
			newRule = false
			break
		}
	}

	// If an existing rule does not exist, this is a new rule so we add it
	if newRule {
		config.Rules = append(config.Rules, rule)
	}

	ruleRe := fmt.Sprintf("(?i)^%s:%s:%s:%s:%s:%s:%s:%s", rule.Match.EventType, rule.Match.Subject, rule.Match.Service, rule.Match.System, rule.Match.Environment, rule.Match.Host, rule.Match.Source, rule.Match.Resource)

	for k, v := range rule.Match.Data {
		ruleRe = fmt.Sprintf("%s(:%s-%s){1}", ruleRe, k, v)
	}

	ruleRe = fmt.Sprintf("%s:.*:$", ruleRe)

	// The rule may be a modification of an existing rule.
	// So we need to seach the map to see if there is an existing entry with the same name, and if there is we delete it, prior to adding the new rule definition.
	for k, r := range config.RuleMatches {
		if strings.EqualFold(r, rule.Name) {
			delete(config.RuleMatches, k)
			break
		}
	}
	config.RuleMatches[ruleRe] = rule.Name

	rulesJson, err := json.Marshal(config.Rules)
	if err != nil {
		logger.Errorw("unable to convert loaded rule definitions to JSON",
			"error", err,
		)
	}

	ruleMatchesJson, err := json.Marshal(config.RuleMatches)
	if err != nil {
		logger.Errorw("unable to convert loaded rule definitions to JSON",
			"error", err,
		)
	}

	config.RulesJson = string(rulesJson)
	config.RuleMatchesJson = string(ruleMatchesJson)
}
