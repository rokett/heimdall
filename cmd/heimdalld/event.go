package main

import (
	"time"
)

// Event represents an internal event via which all routing will happen.
// Incoming messages are converted into the Event format.
type Event struct {
	ID              int64             `json:"id" db:"id"`
	Source          string            `json:"source" db:"source"`
	Type            string            `json:"type" db:"type"`
	Severity        string            `json:"severity" db:"severity"`
	Status          string            `json:"status" db:"status"`
	LifetimeCount   int64             `json:"lifetime_count" db:"lifetime_count"`
	RecurrenceCount int64             `json:"recurrence_count" db:"recurrence_count"`
	Subject         string            `json:"subject" db:"subject"`
	Service         string            `json:"service" db:"service"`
	System          string            `json:"system" db:"system"`
	Environment     string            `json:"environment" db:"environment"`
	Host            string            `json:"host" db:"host"`
	Resource        string            `json:"resource" db:"resource"`
	Correlate       []string          `json:"correlate"`
	Data            map[string]string `json:"data" db:"data"`
	FingerprintRaw  []string          `json:"-"`
	Fingerprint     string            `json:"-"`
	CreatedAt       time.Time         `json:"created_at" db:"created_at"`
	UpdatedAt       time.Time         `json:"updated_at" db:"updated_at"`

	// The following fields are for incoming events only; they are not stored in the database
	Time time.Time `json:"time"`
}
