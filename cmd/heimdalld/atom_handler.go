package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type atomHandler struct {
	URL                          string `json:"-"`
	Script                       string `json:"script"`
	Parameters                   string `json:"parameters"`
	Scope                        string `json:"scope"`
	Executor                     string `json:"executor"`
	ExecutionUsername            string `json:"execution_username"`
	ExpireQueuedJobAfterDuration string `json:"expire_queued_job_after_duration"`
}

func (h atomHandler) run(ctx context.Context, config *config, isTest bool) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"atomHandler.run",
	)
	defer span.End()

	payload, err := json.Marshal(h)
	if err != nil {
		span.SetStatus(codes.Error, "converting payload to JSON")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	span.SetAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
		attribute.Bool("handler.isTest", isTest),
	)

	if isTest {
		return
	}

	client := &http.Client{
		Timeout: time.Second * 20,
	}

	req, err := http.NewRequest("POST", h.URL, bytes.NewBuffer(payload))
	if err != nil {
		span.SetStatus(codes.Error, "creating HTTP request")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("vnd-atom-application-token", config.AuthTokens["atom"])

	span.AddEvent("sending payload to Atom", trace.WithAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
	))

	resp, err := client.Do(req)
	if err != nil {
		span.SetStatus(codes.Error, "sending event")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		span.SetAttributes(attribute.Int("handler.response_status_code", resp.StatusCode))
		span.SetStatus(codes.Error, "reading response body")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	span.SetAttributes(
		attribute.String("handler.response", string(body)),
		attribute.Int("handler.response_status_code", resp.StatusCode),
	)

	switch resp.StatusCode {
	case 202:
		span.AddEvent("payload accepted")
	default:
		span.SetStatus(codes.Error, "payload rejected")
	}
}
