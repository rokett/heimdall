package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io"
	"net/http"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type removeRuleRequest struct {
	Name string `json:"name"`
}

func (app *application) removeRule(w http.ResponseWriter, r *http.Request) {
	_, span := app.tracer.Start(
		r.Context(),
		"removeRule",
	)
	defer span.End()

	defer r.Body.Close()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		span.SetStatus(codes.Error, "reading HTTP request body")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to read HTTP request body; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))

		return
	}
	span.SetAttributes(attribute.String("http.request.body", string(body)))

	var rule removeRuleRequest

	err = decodeJSON(body, &rule)
	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			span.SetStatus(codes.Error, mr.msg)
			span.RecordError(err, trace.WithStackTrace(true))

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(mr.msg))
		} else {
			span.SetStatus(codes.Error, "decoding rule")
			span.RecordError(err, trace.WithStackTrace(true))

			msg := fmt.Sprintf("unable to decode rule; %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(msg))
		}
		return
	}

	span.SetAttributes(attribute.String("rule.name", rule.Name))

	_, err = app.db.Exec("DELETE FROM rules WHERE name = @p1", rule.Name)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		span.SetStatus(codes.Error, "deleting rule definition")
		span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("DELETE FROM rules WHERE name = '%s'", rule.Name)))
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to delete rule definition for %s from database; %s", rule.Name, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	err = app.ec.Publish("rule_removed", rule.Name)
	if err != nil {
		span.SetStatus(codes.Error, "publishing deleted rule")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to publish removed rule, %s; %s", rule.Name, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	span.AddEvent("rule removed")

	w.WriteHeader(http.StatusOK)
}
