package main

import (
	"net"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) getClientIP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		span := trace.SpanFromContext(r.Context())

		// Retrieve the client IP from the remote address of the request
		clientIP, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			span.SetStatus(codes.Error, "retrieving 'RemoteAddr' HTTP header")
			span.RecordError(err, trace.WithStackTrace(true))

			clientIP = "0.0.0.0"
		}

		// If the request has passed through a proxy, the RemoteAddr may actually end up being the IP of the proxy.
		// Depending on how the proxy is configured, the X-Forwarded-For header may have been inserted to show the REAL client IP.
		// If it's there, we'll grab it and use it.
		// X-Forwarded-For could be a comma separated list, so we'll split it and then select the first entry which is the real client IP.
		// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For for more info.
		if r.Header.Get("X-Forwarded-For") != "" {
			xForwardedFor := strings.Split(r.Header.Get("X-Forwarded-For"), ",")
			clientIP = strings.TrimSpace(xForwardedFor[0])

			span.SetAttributes(attribute.String("http.request.header.x_forwarded_for", r.Header.Get("X-Forwarded-For")))
		}

		span.SetAttributes(attribute.String("http.client_ip", clientIP))

		next.ServeHTTP(w, r)
	})
}

func (app *application) tracing(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, span := app.tracer.Start(
			r.Context(),
			chi.RouteContext(r.Context()).RoutePattern(),
			trace.WithAttributes(semconv.NetAttributesFromHTTPRequest("tcp", r)...),
			trace.WithAttributes(semconv.EndUserAttributesFromHTTPRequest(r)...),
			trace.WithAttributes(semconv.HTTPServerAttributesFromHTTPRequest("heimdall", r.URL.String(), r)...),
			trace.WithSpanKind(trace.SpanKindServer),
			trace.WithAttributes(
				semconv.HTTPRouteKey.String(r.URL.String()),
				attribute.String("http.request.header.referer", r.Header.Get("Referer")),
			),
		)
		defer span.End()

		next.ServeHTTP(w, r.WithContext(ctx))

		span.SetAttributes(
			attribute.String("http.response.header.x_xss_protection", w.Header().Get("X-XSS-Protection")),
			attribute.String("http.response.header.x_frame_options", w.Header().Get("X-Frame-Options")),
			attribute.String("http.response.header.cache_control", w.Header().Get("Cache-Control")),
		)
	})
}
