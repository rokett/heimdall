package main

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func getSingleRule(id int64, db *sqlx.DB) (ruleDefinition, error) {
	var ruleDef ruleDefinition

	err := db.Get(&ruleDef, "SELECT id, definition FROM rules WHERE id = @p1", id)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return ruleDef, fmt.Errorf("unable to retrieve rule definition: %w", err)
	}

	return ruleDef, nil
}
