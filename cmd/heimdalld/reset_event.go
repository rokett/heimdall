package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) resetEvent(w http.ResponseWriter, r *http.Request) {
	_, span := app.tracer.Start(
		r.Context(),
		"resetEvent",
	)
	defer span.End()

	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		span.SetStatus(codes.Error, "parsing id of event to reset")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to parse id of event to reset; %s", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(msg))

		return
	}
	span.SetAttributes(attribute.Int64("event.id", id))

	now := time.Now().UTC().Format(time.RFC3339)

	_, err = app.db.Exec("UPDATE events SET status = 'resolved', recurrence_count = 0, updated_at = @p1 WHERE id = @p2", now, id)
	if err != nil {
		span.SetStatus(codes.Error, "resetting count on event")
		span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("UPDATE events SET status = 'resolved', recurrence_count = 0, updated_at = '%s' WHERE id = %d", now, id)))
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("error resetting count on existing event; %s", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(msg))

		return
	}

	span.AddEvent("event count reset")

	w.WriteHeader(http.StatusOK)
}
