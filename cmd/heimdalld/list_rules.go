package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) listRules(w http.ResponseWriter, r *http.Request) {
	_, span := app.tracer.Start(
		r.Context(),
		"listRules",
	)
	defer span.End()

	var rules []rule

	err := app.db.Select(&rules, "SELECT name FROM rules")
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		span.SetStatus(codes.Error, "list rules")
		span.SetAttributes(attribute.String("db.statement", "SELECT name FROM rules"))
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to list rules; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	payload, err := json.Marshal(rules)
	if err != nil {
		span.SetStatus(codes.Error, "marshal rules")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to marshal rules to json string; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}
