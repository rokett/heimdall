package main

import (
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

func setupNATSClient(natsConnString string, logger *zap.SugaredLogger) (nc *nats.Conn, ec *nats.EncodedConn) {
	totalWait := 10 * time.Minute
	reconnectDelay := 10 * time.Second
	connTimeout := 5 * time.Second

	connOpts := []nats.Option{
		nats.RetryOnFailedConnect(true),
		nats.ReconnectWait(reconnectDelay),
		nats.MaxReconnects(int(totalWait / reconnectDelay)),
		nats.Timeout(connTimeout),
		nats.DisconnectErrHandler(func(_ *nats.Conn, err error) {
			if err != nil {
				logger.Warnw(fmt.Sprintf("Disconnected from NATS server: will attempt reconnects for %.0fm", totalWait.Minutes()),
					"error", err,
				)
			}
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			logger.Infow("Connected to NATS server",
				"nats_server", nc.ConnectedUrl(),
			)
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			if nc.LastError() != nil {
				logger.Errorw("Failed to reconnect to NATS server; stopping attempts",
					"error", nc.LastError(),
				)
			}
		}),
	}

	nc, err := nats.Connect(natsConnString, connOpts...)
	if err != nil {
		logger.Fatalw("unable to connect to NATS server",
			"error", err,
			"nats_server", natsConnString,
		)
	}

	ec, err = nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		nc.Close()

		logger.Fatalw("unable to set encoded NATS connection",
			"error", err,
			"nats_server", natsConnString,
		)
	}

	return nc, ec
}
