package main

import (
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

func loadRules(db *sqlx.DB, logger *zap.SugaredLogger, config *config) {
	var rules []ruleDefinition

	err := db.Select(&rules, "SELECT id, definition FROM rules")
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		logger.Fatalw("unable to retrieve rule definitions",
			"error", err,
		)
	}

	for _, r := range rules {
		parseRuleDefinition(r, logger, config)
	}
}
