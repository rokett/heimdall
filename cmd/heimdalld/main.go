package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

var (
	app     = "Heimdall"
	version string
	build   string

	versionFlg               = flag.Bool("version", false, "Display application version")
	logFileFlg               = flag.String("log_file", "", "Name of file to log application entries to")
	portFlg                  = flag.Int("port", 10002, "Port to listen on")
	natsHostFlg              = flag.String("nats-host", "127.0.0.1", "IP address/FQDN of NATS server")
	natsServerPortFlg        = flag.Int("nats_server_port", 4222, "Port for NATS to listen for clients on")
	natsClusterPortFlg       = flag.Int("nats_cluster_port", 4248, "Port for NATS to listen for cluster traffic on")
	natsClusterRoutesFlg     = flag.String("nats_cluster_routes", "", "List of NATS cluster hosts")
	dbHostFlg                = flag.String("db_host", "127.0.0.1", "Name of database host, including instance name if needed")
	dbPortFlg                = flag.Int("db_port", 1433, "Port to connect to database server on")
	dbName                   = flag.String("db", "heimdall", "Name of the database to use")
	dbUsernameFlg            = flag.String("db_username", "", "Username of account with read/write access to database")
	dbPasswordFlg            = flag.String("db_password", "", "Password for database user")
	emailHostFlg             = flag.String("email_host", "", "SMTP host")
	emailPortFlg             = flag.Int("email_port", 25, "SMTP port")
	emailFromFlg             = flag.String("email_from_address", "", "From address for email sent from Heimdall")
	authTokens               = flag.String("auth_tokens", "", "Comma separated list of event handler authentication tokens in key=value format")
	corsAllowedOriginsFlg    = flag.String("cors_allowed_origins", "", "Allowed origins for CORS purposes")
	corsAllowedHeadersFlg    = flag.String("cors_allowed_headers", "*", "Allowed headers for CORS purposes")
	tracingEndpointFlg       = flag.String("tracing.endpoint", "", "Tracing endpoint in the format IP:Port")
	tracingEndpointSecureFlg = flag.Bool("tracing.endpoint_secure", false, "Set to true if the tracing endpoint supports TLS connections")
	deploymentEnvFlg         = flag.String("deployment.environment", "", "The environment this application is running under; generally either `test` or `production`.")
	httpRequestTimeout       = flag.Int("http.request_timeout", 5, "HTTP request timeout in seconds")
	helpFlg                  = flag.Bool("help", false, "Display application help")
)

func main() {
	flag.Parse()

	if *versionFlg {
		fmt.Printf("%s v%s build %s\n", app, version, build)
		os.Exit(0)
	}

	if *helpFlg {
		flag.PrintDefaults()
		os.Exit(0)
	}

	zapLogger, logger := setupLogger(app, version, build, *logFileFlg)
	defer zapLogger.Sync()
	defer logger.Sync()

	config, err := parseConfig(
		*portFlg,
		*natsHostFlg,
		*natsServerPortFlg,
		*natsClusterPortFlg,
		*natsClusterRoutesFlg,
		*dbHostFlg,
		*dbPortFlg,
		*dbName,
		*dbUsernameFlg,
		*dbPasswordFlg,
		*emailHostFlg,
		*emailPortFlg,
		*emailFromFlg,
		*authTokens,
		*corsAllowedOriginsFlg,
		*corsAllowedHeadersFlg,
		*tracingEndpointFlg,
		*tracingEndpointSecureFlg,
		*deploymentEnvFlg,
		*httpRequestTimeout,
		logger,
	)
	if err != nil {
		os.Exit(1)
	}

	ctx := context.Background()

	//Initialise tracer
	tp := setupTracer(ctx, app, version, build, logger, config)
	otel.SetTracerProvider(tp)
	defer func() {
		fmt.Println("shutdown")
		err := tp.Shutdown(ctx)
		if err != nil {
			logger.Errorw("shutting down tracer provider",
				"error", err,
			)
		}
	}()

	db := openDB(config.Database.Host, config.Database.Port, config.Database.Name, config.Database.Username, config.Database.Password, logger)
	defer db.Close()

	err = runMigrations(logger, db)
	if err != nil {
		os.Exit(1)
	}

	loadRules(db, logger, &config)

	// The servers run an embedded NATS server, in a cluster if desired, to allow messages to be published to each other.
	// Let's run the server and setup the client so that messages can be published and received.
	runNATSServer(config.NatsServerPort, config.NatsClusterPort, config.NatsClusterRoutes, logger)
	nc, ec := setupNATSClient(config.NatsConnString, logger)
	defer nc.Close()
	defer ec.Close()

	setupSubscriptions(&config, ec, logger, db)

	app := &application{
		config: config,
		db:     db,
		ec:     ec,
		logger: logger,
		tracer: tp.Tracer(app),
	}

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", app.config.Port),
		ErrorLog:     zap.NewStdLog(zapLogger),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  app.config.HTTPRequestTimeout,
		WriteTimeout: 10 * time.Second,
	}

	app.logger.Infow("running server",
		"port", app.config.Port,
	)

	err = srv.ListenAndServe()
	app.logger.Fatal(err.Error())
}
