package main

import (
	"time"
)

type rule struct {
	Name                         string        `json:"name"`
	Match                        match         `json:"match"`
	BoostScore                   int           `json:"boost_score"`
	Runbook                      string        `json:"runbook"`
	ClearRecurrenceAfter         string        `json:"clear_recurrence_after"`
	ClearRecurrenceAfterDuration time.Duration `json:"-"`
	Actions                      []action      `json:"actions"`
	Score                        int           `json:"-"`
}

type match struct {
	EventType   string            `json:"event_type"`
	Subject     string            `json:"subject"`
	Service     string            `json:"service"`
	System      string            `json:"system"`
	Environment string            `json:"environment"`
	Host        string            `json:"host"`
	Source      string            `json:"source"`
	Resource    string            `json:"resource"`
	Data        map[string]string `json:"data"`
}

type action struct {
	Recurrence int64     `json:"recurrence"`
	Handlers   []handler `json:"handlers"`
}

type handler struct {
	Action     string            `json:"action"`
	Parameters map[string]string `json:"parameters"`
	Overrides  overrides         `json:"overrides"`
}

type overrides struct {
	FromAddress string `json:"from_address"`
	Message     string `json:"message"`
	Severity    string `json:"severity"`
	SMTPHost    string `json:"smtp_host"`
	SMTPPort    int    `json:"smtp_port"`
	Status      string `json:"status"`
	Type        string `json:"type"`
}
