package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (app *application) routes() http.Handler {
	r := chi.NewRouter()

	if len(app.config.CorsAllowedOrigins) > 0 {
		r.Use(cors.Handler(cors.Options{
			AllowedOrigins: app.config.CorsAllowedOrigins,
			AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
			AllowedHeaders: app.config.CorsAllowedHeaders,
		}))
	}

	r.Method(http.MethodGet, "/metrics", promhttp.Handler())

	// status route is used for healthchecking and is excluded from logging
	r.Get("/status", func(w http.ResponseWriter, r *http.Request) {
		err := app.db.Ping()
		if err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
	})

	r.Group(func(r chi.Router) {
		r.Use(app.tracing)
		r.Use(app.getClientIP)
		r.Use(middleware.Compress(5))

		r.Get("/rules", app.listRules)
		r.Put("/rules", app.upsertRule)
		r.Delete("/rules", app.removeRule)
		r.Post("/rules/validate", app.validateRule)
		r.Post("/event", app.processIncomingEvents)
		r.Post("/resetEvent/{id}", app.resetEvent)
	})

	return r
}
