package main

import (
	"context"
	"fmt"
	"net/mail"
	"net/smtp"
	"strings"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type emailHandler struct {
	SMTPHost string
	SMTPPort int
	To       []string
	From     string
	Subject  string
	Body     string
}

func (h emailHandler) run(ctx context.Context, config *config, isTest bool) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"emailHandler.run",
	)
	defer span.End()

	smtpAddr := fmt.Sprintf("%s:%d", h.SMTPHost, h.SMTPPort)

	from := mail.Address{
		Name:    "Heimdall",
		Address: h.From,
	}

	var emailHeaderTo []string
	var rcpts []mail.Address

	for _, v := range h.To {
		to := mail.Address{
			Name:    "",
			Address: v,
		}

		emailHeaderTo = append(emailHeaderTo, to.String())
		rcpts = append(rcpts, to)
	}

	// Setup headers
	headers := map[string]string{
		"From":    from.String(),
		"To":      strings.Join(emailHeaderTo, ","),
		"Subject": h.Subject,
	}

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + h.Body

	span.SetAttributes(
		attribute.StringSlice("handler.email_to", h.To),
		attribute.String("handler.email_from", h.From),
		attribute.String("handler.smtp_address", smtpAddr),
		attribute.String("handler.email_contents", message),
		attribute.Bool("handler.isTest", isTest),
	)

	if isTest {
		return
	}

	c, err := smtp.Dial(smtpAddr)
	if err != nil {
		span.SetStatus(codes.Error, "dialing SMTP server")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	for _, to := range rcpts {
		if err = c.Mail(from.Address); err != nil {
			span.SetStatus(codes.Error, "setting FROM address")
			span.RecordError(err, trace.WithStackTrace(true))

			continue
		}

		if err = c.Rcpt(to.Address); err != nil {
			span.SetStatus(codes.Error, "setting TO address")
			span.RecordError(err, trace.WithStackTrace(true))

			continue
		}

		w, err := c.Data()
		if err != nil {
			span.SetStatus(codes.Error, "sending DATA command to email server")
			span.RecordError(err, trace.WithStackTrace(true))

			continue
		}

		_, err = w.Write([]byte(message))
		if err != nil {
			span.SetStatus(codes.Error, "writing email")
			span.RecordError(err, trace.WithStackTrace(true))

			continue
		}

		err = w.Close()
		if err != nil {
			span.SetStatus(codes.Error, "closing email writer")
			span.RecordError(err, trace.WithStackTrace(true))
		}
	}

	err = c.Quit()
	if err != nil {
		span.SetStatus(codes.Error, "quitting SMTP client")
		span.RecordError(err, trace.WithStackTrace(true))

		return
	}

	span.AddEvent("sent email")
}
