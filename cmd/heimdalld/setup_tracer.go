package main

import (
	"context"
	"os"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.uber.org/zap"
)

func setupTracer(ctx context.Context, app string, version string, build string, logger *zap.SugaredLogger, config config) *sdktrace.TracerProvider {
	exp, err := newTraceExporter(ctx, config)
	if err != nil {
		logger.Fatalw("initialising trace exporter",
			"error", err,
		)
	}

	hostname, err := os.Hostname()
	if err != nil {
		logger.Fatalw("error getting worker hostname",
			"error", err,
		)
	}

	tp, err := newTracerProvider(ctx, app, version, build, hostname, config.DeploymentEnv, exp)
	if err != nil {
		logger.Fatalw("initialising tracer provider",
			"error", err,
		)
	}

	return tp
}

func newTraceExporter(ctx context.Context, config config) (*otlptrace.Exporter, error) {
	var client otlptrace.Client

	if config.Tracing.Secure {
		client = otlptracehttp.NewClient(
			otlptracehttp.WithEndpoint(config.Tracing.Endpoint),
		)
	} else {
		client = otlptracehttp.NewClient(
			otlptracehttp.WithInsecure(),
			otlptracehttp.WithEndpoint(config.Tracing.Endpoint),
		)
	}

	exporter, err := otlptrace.New(ctx, client)
	if err != nil {
		return nil, err
	}

	return exporter, nil
}

func newTracerProvider(ctx context.Context, app string, version string, build string, hostname string, deploymentEnv string, exp sdktrace.SpanExporter) (*sdktrace.TracerProvider, error) {
	r, err := resource.New(
		ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(app),
			semconv.ServiceVersionKey.String(version),
			semconv.HostNameKey.String(hostname),
			attribute.String("service.build", build),
			semconv.DeploymentEnvironmentKey.String(deploymentEnv),
		),
	)
	if err != nil {
		return nil, err
	}

	return sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(r),
	), nil
}
