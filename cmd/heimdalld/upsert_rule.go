package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) upsertRule(w http.ResponseWriter, r *http.Request) {
	_, span := app.tracer.Start(
		r.Context(),
		"upsertRule",
	)
	defer span.End()

	defer r.Body.Close()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		span.SetStatus(codes.Error, "reading HTTP request body")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to read HTTP request body; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))

		return
	}
	span.SetAttributes(attribute.String("http.request.body", string(body)))

	var rule rule

	// We decode the incoming JSON formatted rule to ensure it is valid
	err = decodeJSON(body, &rule)
	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			span.SetStatus(codes.Error, mr.msg)
			span.RecordError(err, trace.WithStackTrace(true))

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(mr.msg))
		} else {
			span.SetStatus(codes.Error, "decoding rule")
			span.RecordError(err, trace.WithStackTrace(true))

			msg := fmt.Sprintf("unable to decode rule; %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(msg))
		}

		return
	}

	err = isRuleValid(rule)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		span.RecordError(err, trace.WithStackTrace(true))

		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	span.SetAttributes(attribute.String("rule.name", rule.Name))

	// The incoming JSON may not be compressed, so if we marshal the struct back to JSON we get a compressed string for storing in the DB
	js, err := json.Marshal(rule)
	if err != nil {
		span.SetStatus(codes.Error, "marshaling rule to JSON string")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to marshal rule to json string; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	span.SetAttributes(attribute.String("rule.definition", string(js)))

	// If a rule definition exists with the same name then we'll update that record, otherwise we'll insert a new one.
	// All rule names MUST be unique.
	var id int64

	err = app.db.Get(&id, "SELECT id FROM rules WHERE name = @p1", rule.Name)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		span.SetStatus(codes.Error, "checking for existing rule")
		span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("SELECT id FROM rules WHERE name = '%s'", rule.Name)))
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to check for existing rule; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	if id == 0 {
		err = app.db.Get(&id, "INSERT INTO rules (name, definition) VALUES (@p1, @p2);SELECT id = convert(bigint, SCOPE_IDENTITY());", rule.Name, string(js))
		if err != nil {
			span.SetStatus(codes.Error, "inserting new rule")
			span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("INSERT INTO rules (name, definition) VALUES ('%s', '%s');SELECT id = convert(bigint, SCOPE_IDENTITY());", rule.Name, string(js))))
			span.RecordError(err, trace.WithStackTrace(true))

			msg := fmt.Sprintf("unable to add new rule; %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(msg))
			return
		}
	} else {
		_, err = app.db.Exec("UPDATE rules SET definition = @p1 WHERE id = @p2", string(js), id)
		if err != nil {
			span.SetStatus(codes.Error, "updating existing rule")
			span.SetAttributes(attribute.String("db.statement", fmt.Sprintf("UPDATE rules SET definition = '%s' WHERE id = %d", string(js), id)))
			span.RecordError(err, trace.WithStackTrace(true))

			msg := fmt.Sprintf("unable to update existing rule; %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(msg))
			return
		}
	}

	span.SetAttributes(attribute.Int64("rule.id", id))

	err = app.ec.Publish("rule_updated", id)
	if err != nil {
		span.SetStatus(codes.Error, "publishing rule")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to publish updated rule; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))
		return
	}

	span.AddEvent("upserted rule")

	w.WriteHeader(http.StatusAccepted)
}
