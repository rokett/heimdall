package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime/debug"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

func setupLogger(app string, version string, build string, logFile string) (*zap.Logger, *zap.SugaredLogger) {
	w := zapcore.AddSync(os.Stdout)

	if logFile != "" {
		if filepath.Ext(logFile) == "" {
			logFile = fmt.Sprintf("%s.log", logFile)
		}

		w = zapcore.AddSync(&lumberjack.Logger{
			Filename:   logFile,
			MaxSize:    100,
			MaxBackups: 3,
			MaxAge:     7,
			Compress:   false,
		})
	}

	// Need to create a new encoder config in order to print the timestamp in RFC339 format
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoder := zapcore.NewJSONEncoder(encoderConfig)

	// Now we can create a new Zap core using the custom encoder, the custom writer, and setting the default level
	level := zap.InfoLevel

	var goarch string
	var goos string
	buildInfo, ok := debug.ReadBuildInfo()
	if ok {
		for _, v := range buildInfo.Settings {
			if v.Key == "GOARCH" {
				goarch = v.Value
				continue
			}
			if v.Key == "GOOS" {
				goos = v.Value
				continue
			}
		}
	}

	zapCore := zapcore.NewCore(encoder, w, level).With([]zapcore.Field{
		zap.String("app", app),
		zap.String("version", version),
		zap.String("build", build),
		zap.String("go_version", buildInfo.GoVersion),
		zap.String("goarch", goarch),
		zap.String("goos", goos),
	})

	// And finally create the logger, including the caller details and stacktrace
	zapLogger := zap.New(zapCore, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))

	return zapLogger, zapLogger.Sugar()
}
