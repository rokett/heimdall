package main

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

func isRuleValid(rule rule) error {
	var validationErrors []string

	if rule.Name == "" {
		validationErrors = append(validationErrors, "name -> MUST be defined")
	}

	if rule.Runbook == "" {
		validationErrors = append(validationErrors, "runbook -> MUST be defined")
	}

	// Runbook MUST be a valid URL with the article being the same as the rule name, but in lowercase and with _ replacing spaces
	articleName := strings.ToLower(rule.Name)
	articleName = strings.ReplaceAll(articleName, " ", "_")
	if rule.Runbook != fmt.Sprintf("https://itops-wiki.exe.nhs.uk/issues/%s", articleName) {
		validationErrors = append(validationErrors, "runbook -> MUST be a valid URL and the article name MUST match the rule name, but in lowercase and with spaces replaced by '_'")
	}

	if len(rule.Actions) == 0 {
		validationErrors = append(validationErrors, "actions -> at least one action MUST be defined")
	}

	if rule.Match.EventType == "" {
		validationErrors = append(validationErrors, "match:event_type -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Subject == "" {
		validationErrors = append(validationErrors, "match:subject -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Service == "" {
		validationErrors = append(validationErrors, "match:service -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.System == "" {
		validationErrors = append(validationErrors, "match:system -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Environment == "" {
		validationErrors = append(validationErrors, "match:environment -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Host == "" {
		validationErrors = append(validationErrors, "match:host -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Source == "" {
		validationErrors = append(validationErrors, "match:source -> MUST be defined; wildcards are allowed")
	}

	if rule.Match.Resource == "" {
		validationErrors = append(validationErrors, "match:resource -> MUST be defined; wildcards are allowed")
	}

	clearRecurrenceAfterDuration, err := time.ParseDuration(rule.ClearRecurrenceAfter)
	if err != nil {
		msg := fmt.Sprintf("clear_recurrence_after -> %s\n", err)
		validationErrors = append(validationErrors, msg)
	}
	rule.ClearRecurrenceAfterDuration = clearRecurrenceAfterDuration

	if len(validationErrors) > 0 {
		// Each error should be on its own line, preceded by `- `
		msg := strings.Join(validationErrors, "\n - ")

		return errors.New(msg)
	}

	return nil
}
