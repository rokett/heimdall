package main

import (
	"fmt"
	"io"
	"net/http"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (app *application) processIncomingEvents(w http.ResponseWriter, r *http.Request) {
	ctx, span := app.tracer.Start(
		r.Context(),
		"processIncomingEvents",
	)
	defer span.End()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		span.SetStatus(codes.Error, "reading HTTP request body")
		span.RecordError(err, trace.WithStackTrace(true))

		msg := fmt.Sprintf("unable to read HTTP request body; %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(msg))

		r.Body.Close() // Explicitly closing the body here as we don't want to wait for each event to be processed

		return
	}
	r.Body.Close() // Explicitly closing the body here as we don't want to wait for each event to be processed
	span.SetAttributes(
		attribute.String("http.request.body", string(body)),
		attribute.String("event.request_source", r.URL.Query().Get("source")),
	)

	var events []Event

	source := r.URL.Query().Get("source")
	switch source {
	case "alertmanager":
		events, err = processAlertManagerPayload(ctx, body)
	case "atom":
		events, err = processAtomPayload(ctx, body)
	case "cloudevent":
		events, err = processCloudEventsPayload(ctx, body)
	case "vrli":
		events, err = processVrliPayload(ctx, body)
	default:
		events, err = processNativePayload(ctx, body)
	}
	if err != nil {
		span.SetStatus(codes.Error, "decoding event payload")
		span.RecordError(err, trace.WithStackTrace(true))

		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))

		return
	}

	var isTest bool
	if r.URL.Query().Has("test") {
		isTest = true
	}
	span.SetAttributes(attribute.Bool("event.isTest", isTest))

	// We can tell the caller that the event has been accepted, and continue processing the event without making them wait.
	w.WriteHeader(http.StatusAccepted)

	for _, event := range events {
		go app.processEvent(ctx, event, isTest)
	}
}
