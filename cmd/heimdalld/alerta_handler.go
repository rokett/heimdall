package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

type alertaHandler struct {
	URL         string            `json:"-"`
	Resource    string            `json:"resource"`
	Event       string            `json:"event"`
	Environment string            `json:"environment,omitempty"`
	Severity    string            `json:"severity,omitempty"`
	Correlate   []string          `json:"correlate,omitempty"`
	Status      string            `json:"status,omitempty"`
	Service     []string          `json:"service,omitempty"`
	Group       string            `json:"group,omitempty"`
	Value       string            `json:"value,omitempty"`
	Text        string            `json:"text,omitempty"`
	Tags        []string          `json:"tags,omitempty"`
	Attributes  map[string]string `json:"attributes,omitempty"`
	Origin      string            `json:"origin,omitempty"`
	EventType   string            `json:"type,omitempty"`
	Timeout     int               `json:"timeout,omitempty"`
	RawData     string            `json:"rawData,omitempty"`
}

func (h alertaHandler) run(ctx context.Context, config *config, isTest bool) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"alertaHandler.run",
	)
	defer span.End()

	h.EventType = "Heimdall"

	payload, err := json.Marshal(h)
	if err != nil {
		span.SetStatus(codes.Error, "converting payload to JSON")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	span.SetAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
		attribute.Bool("handler.isTest", isTest),
	)

	if isTest {
		return
	}

	client := &http.Client{
		Timeout: time.Second * 20,
	}

	req, err := http.NewRequest("POST", h.URL, bytes.NewBuffer(payload))
	if err != nil {
		span.SetStatus(codes.Error, "creating HTTP request")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	req.Header.Add("Content-Type", "application/json")

	if config.AuthTokens["alerta"] != "" {
		req.Header.Add("Authorization", fmt.Sprintf("Key %s", config.AuthTokens["alerta"]))
	}

	span.AddEvent("sending payload to Alerta", trace.WithAttributes(
		attribute.String("handler.url", h.URL),
		attribute.String("handler.payload", string(payload)),
	))

	resp, err := client.Do(req)
	if err != nil {
		span.SetStatus(codes.Error, "sending event")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		span.SetAttributes(attribute.Int("handler.response_status_code", resp.StatusCode))
		span.SetStatus(codes.Error, "reading response body")
		span.RecordError(err, trace.WithStackTrace(true))
		return
	}

	span.SetAttributes(
		attribute.String("handler.response", string(body)),
		attribute.Int("handler.response_status_code", resp.StatusCode),
	)

	switch resp.StatusCode {
	case 201, 202:
		span.AddEvent("payload accepted")
	default:
		span.SetStatus(codes.Error, "payload rejected")
	}
}
