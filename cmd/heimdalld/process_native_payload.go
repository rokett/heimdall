package main

import (
	"context"
	"encoding/json"
	"fmt"

	"go.opentelemetry.io/otel"
	"rokett.me/heimdall/internal/fingerprint"
)

func processNativePayload(ctx context.Context, srcEvent []byte) (events []Event, err error) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"processNativePayload",
	)
	defer span.End()

	var event Event

	err = json.Unmarshal(srcEvent, &event)
	if err != nil {
		return events, fmt.Errorf("unable to decode native event: %w", err)
	}

	// Now we need to build a fingerprint to help us identify duplicate events
	event.FingerprintRaw = []string{
		event.Resource,
		event.Host,
		event.System,
		event.Service,
		event.Environment,
		event.Type,
		event.Subject,
	}

	event.Fingerprint, err = fingerprint.Calculate(event.FingerprintRaw)
	if err != nil {
		return events, fmt.Errorf("unable to calculate fingerprint: %w", err)
	}

	events = append(events, event)

	return events, nil
}
