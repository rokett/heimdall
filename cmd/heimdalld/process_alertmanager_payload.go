package main

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"time"

	"go.opentelemetry.io/otel"
	"rokett.me/heimdall/internal/fingerprint"
)

// AlertManagerPayload represents the expected JSON structure received from AlertManager (https://prometheus.io/docs/alerting/latest/notifications/)
type AlertManagerPayload struct {
	Receiver          string            `json:"receiver"`
	Status            string            `json:"status"`
	GroupLabels       map[string]string `json:"groupLabels"`
	CommonLabels      map[string]string `json:"commonLabels"`
	CommonAnnotations map[string]string `json:"commonAnnotations"`
	ExternalURL       string            `json:"externalURL"`
	Version           string            `json:"version"`
	GroupKey          string            `json:"groupKey"`
	Alerts            []struct {
		Status       string            `json:"status"`
		Labels       map[string]string `json:"labels"`
		Annotations  map[string]string `json:"annotations"`
		StartsAt     time.Time         `json:"startsAt"`
		EndsAt       time.Time         `json:"endsAt"`
		GeneratorURL string            `json:"generatorURL"`
		Fingerprint  string            `json:"fingerprint"`
	} `json:"alerts"`
}

func processAlertManagerPayload(ctx context.Context, srcEvent []byte) (events []Event, err error) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"processAlertManagerPayload",
	)
	defer span.End()

	var event AlertManagerPayload

	err = json.Unmarshal(srcEvent, &event)
	if err != nil {
		return events, fmt.Errorf("unable to decode Alertmanager event: %w", err)
	}

	for _, al := range event.Alerts {
		// evDetails contain extracted details from the AlertManager payload that we use to build the Event.
		evDetails := make(map[string]string)

		// mappings contains the specific fields from the AlertManager payload which we want to pull out as part of the event.
		mappings := map[string]bool{
			"alertname":   true,
			"instance":    true,
			"resource":    true,
			"service":     true,
			"system":      true,
			"environment": true,
			"host":        true,
			"severity":    true,
		}

		// Looping over the labels in the AM payload, if we have a mapping match we extract that field and then delete the original.
		// Anything left over gets lumped into the `data` field.
		for k, v := range al.Labels {
			if mappings[k] {
				evDetails[k] = v
				delete(al.Labels, k)
			}
		}

		// Whatever is left in the labels (group, common and alert), any annotations (command and alert), plus the generator URL, now get added to the `data` field
		// We do this in the order common -> group -> alert.  By doing so, when name clashes occur, group will replace common and alert will replace group; in order of specificitiy in other words.
		evData := make(map[string]string)
		evData["generatorURL"] = al.GeneratorURL
		for k, v := range event.CommonLabels {
			evData["label_"+k] = v
		}
		for k, v := range event.GroupLabels {
			evData["label_"+k] = v
		}
		for k, v := range al.Labels {
			evData["label_"+k] = v
		}
		for k, v := range event.CommonAnnotations {
			evData["annotation_"+k] = v
		}
		for k, v := range al.Annotations {
			evData["annotation_"+k] = v
		}

		evData["message"] = evData["annotation_summary"]
		delete(evData, "annotation_summary")

		evData["value"] = evData["annotation_value"]
		delete(evData, "annotation_value")

		var correlate []string
		if evData["label_correlate"] != "" {
			correlate = strings.Split(evData["label_correlate"], ",")
			delete(evData, "label_correlate")
		}

		// Now we need to build a fingerprint to help us identify duplicate events
		fingerprintRaw := []string{
			evDetails["resource"],
			evDetails["host"],
			evDetails["system"],
			evDetails["service"],
			evDetails["environment"],
			evDetails["alertname"], //type
			evDetails["instance"],  //subject
		}

		// We need to add event.Data to the checksum calculation.
		// Because this is a map, the iteration order is random, which would make the checksum different every time.
		// So we first extract the keys into a slice, then we sort that slice alphabetically, and then we iterate over the slice (which is the same each time), and pull out the event.Data values.
		keys := make([]string, len(evData))
		for k := range evData {
			// We don't want to use `generatorURL` as the event could have come from any AlertManager instance
			if k == `generatorURL` {
				continue
			}

			// We don't want `message` or `value` either as the rest of the resources are enough to fingerprint an event
			if k == `message` || k == `value` {
				continue
			}

			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			fingerprintRaw = append(fingerprintRaw, fmt.Sprintf("%s-%s", k, evData[k]))
		}

		fingerprint, err := fingerprint.Calculate(fingerprintRaw)
		if err != nil {
			return events, fmt.Errorf("unable to calculate fingerprint: %w", err)
		}

		events = append(events, Event{
			Source:         "alertmanager",
			Type:           evDetails["alertname"],
			Status:         al.Status,
			Subject:        evDetails["instance"],
			Time:           al.StartsAt,
			Resource:       evDetails["resource"],
			Service:        evDetails["service"],
			System:         evDetails["system"],
			Environment:    evDetails["environment"],
			Host:           evDetails["host"],
			Severity:       evDetails["severity"],
			Correlate:      correlate,
			Data:           evData,
			FingerprintRaw: fingerprintRaw,
			Fingerprint:    fingerprint,
		})
	}

	return events, nil
}
