package main

import "errors"

func getRule(name string, rules []rule) (rule, error) {
	for _, r := range rules {
		if name == r.Name {
			return r, nil
		}
	}

	return rule{}, errors.New("unable to retrieve rule")
}
