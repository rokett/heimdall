package main

import (
	"net/url"

	natsserver "github.com/nats-io/nats-server/v2/server"
	"go.uber.org/zap"
)

func runNATSServer(serverPort int, clusterPort int, routes []*url.URL, logger *zap.SugaredLogger) {
	opts := natsserver.Options{
		Host:   "0.0.0.0",
		Port:   serverPort,
		NoSigs: true,
		Cluster: natsserver.ClusterOpts{
			Host: "0.0.0.0",
			Port: clusterPort,
		},
	}

	if len(routes) != 0 {
		opts.Routes = routes
	}

	natsd, err := natsserver.NewServer(&opts)
	if err != nil {
		logger.Fatalw("error starting NATS server",
			"error", err,
		)
	}

	go natsd.Start()
}
