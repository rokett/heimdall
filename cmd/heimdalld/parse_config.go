package main

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"

	"go.uber.org/zap"
)

type config struct {
	Port               int
	NatsHost           string
	NatsServerPort     int
	NatsConnString     string
	NatsClusterPort    int
	NatsClusterRoutes  []*url.URL
	CorsAllowedOrigins []string
	CorsAllowedHeaders []string
	Database           database
	Email              email
	Rules              []rule
	RulesJson          string
	RuleMatches        map[string]string
	RuleMatchesJson    string
	AuthTokens         map[string]string
	Tracing            tracing
	DeploymentEnv      string
	HTTPRequestTimeout time.Duration
}

type database struct {
	Host     string
	Port     int
	Name     string
	Username string
	Password string
}

type email struct {
	Host string
	Port int
	From string
}

type tracing struct {
	Endpoint string
	Secure   bool
}

func parseConfig(port int, natsHost string, natsServerPort int, natsClusterPort int, natsClusterRoutes string, dbHost string, dbPort int, dbName string, dbUsername string, dbPassword string, emailHost string, emailPort int, emailFrom string, authTokens string, corsAllowedOrigins string, corsAllowedHeaders string, tracingEndpoint string, tracingEndpointSecure bool, deploymentEnv string, httpRequestTimeout int, logger *zap.SugaredLogger) (config, error) {
	var natsHosts []*url.URL
	tmp := strings.Split(natsClusterRoutes, ",")

	for _, v := range tmp {
		if strings.TrimSpace(v) == "" {
			continue
		}

		u, err := url.Parse(strings.TrimSpace(v))
		if err != nil {
			logger.Fatalw("unable to parse NATS cluster routes",
				"error", err,
			)
		}
		natsHosts = append(natsHosts, u)
	}

	var allowedOrigins []string
	tmp = strings.Split(corsAllowedOrigins, ",")
	for _, v := range tmp {
		if strings.TrimSpace(v) == "" {
			continue
		}

		allowedOrigins = append(allowedOrigins, strings.TrimSpace(v))
	}

	var allowedHeaders []string
	tmp = strings.Split(corsAllowedHeaders, ",")
	for _, v := range tmp {
		allowedHeaders = append(allowedHeaders, strings.TrimSpace(v))
	}

	if deploymentEnv == "" {
		logger.Fatal("`deployment.environment` flag is required")
	}

	if deploymentEnv != "test" && deploymentEnv != "production" {
		logger.Fatal("`deployment.environment` flag must be either `test` or `production`")
	}

	cfg := config{
		Port:               port,
		NatsHost:           natsHost,
		NatsServerPort:     natsServerPort,
		NatsConnString:     fmt.Sprintf("nats://%s:%d", natsHost, natsServerPort),
		NatsClusterPort:    natsClusterPort,
		NatsClusterRoutes:  natsHosts,
		CorsAllowedOrigins: allowedOrigins,
		CorsAllowedHeaders: allowedHeaders,
		Database: database{
			Host:     dbHost,
			Port:     dbPort,
			Name:     dbName,
			Username: dbUsername,
			Password: dbPassword,
		},
		Email: email{
			Host: emailHost,
			Port: emailPort,
			From: emailFrom,
		},
		RuleMatches: make(map[string]string),
		Tracing: tracing{
			Endpoint: tracingEndpoint,
			Secure:   tracingEndpointSecure,
		},
		DeploymentEnv:      strings.ToLower(deploymentEnv),
		HTTPRequestTimeout: time.Duration(httpRequestTimeout) * time.Second,
	}

	if authTokens != "" {
		tokens := make(map[string]string)
		tmp := strings.Split(authTokens, ",")
		for _, v := range tmp {
			kv := strings.Split(v, "=")

			if len(kv) != 2 {
				logger.Errorw("invalid auth token definition",
					"auth_token", v,
				)

				return cfg, errors.New("invalid auth token definition")
			}

			key := strings.TrimSpace(kv[0])
			val := strings.TrimSpace(kv[1])

			tokens[key] = val
		}

		if len(tokens) > 0 {
			cfg.AuthTokens = tokens
		}
	}

	return cfg, nil
}
