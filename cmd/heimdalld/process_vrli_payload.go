package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"go.opentelemetry.io/otel"
	"rokett.me/heimdall/internal/fingerprint"
)

// VrliPayload represents the expected JSON structure received from VMware vRealize Log Insight
type VrliPayload struct {
	Type     string `json:"type"`
	Subject  string `json:"subject"`
	Source   string `json:"source"`
	Resource string `json:"resource"`
	Service  string `json:"service"`
	Status   string `json:"status"`
	Severity string `json:"severity"`
	Data     struct {
		AnnotationSummary string `json:"annotation_summary"`
		Recomendation     string `json:"recommendation"`
		AlertType         string `json:"alert_type"`
		Url               string `json:"url"`
		Messages          []struct {
			Text      string `json:"text"`
			Timestamp int64  `json:"timestamp"`
			Fields    []struct {
				Name    string `json:"name"`
				Content string `json:"content"`
			} `json:"fields"`
		} `json:"messages"`
	} `json:"data"`
}

func processVrliPayload(ctx context.Context, srcEvent []byte) (events []Event, err error) {
	tracer := otel.GetTracerProvider()
	_, span := tracer.Tracer("Heimdall").Start(
		ctx,
		"processVrliPayload",
	)
	defer span.End()

	var event VrliPayload

	// The payload that comes from vRLI mangles the messages array by escaping speech marks.
	// So we replace the slashes to fix it.  No idea why vRLI does it; maybe it will be fixed one day.
	srcEvent = bytes.Replace(srcEvent, []byte(`\"`), []byte(`"`), -1)

	err = json.Unmarshal(srcEvent, &event)
	if err != nil {
		return events, fmt.Errorf("unable to decode vRLI event: %w", err)
	}

	for _, ev := range event.Data.Messages {
		// evDetails contain extracted details from the vRLI payload (data.messages.fields) that we use to build the Event.
		evDetails := make(map[string]string)

		// mappings contains the specific fields from the vRLI payload which we want to pull out as part of the event.
		mappings := map[string]bool{
			"hostname": true,
			"appname":  true,
		}

		// Looping over the fields in the payload, if we have a mapping match we extract that field and then delete the original.
		// Anything left over gets lumped into the `data` field.
		// First, because the source is a struct, we dump the entirety of that slice into a map.
		// We then iterate over that map to extract fields.  We do this as it is easier to delete an element from a map, than it is a slice.
		evData := make(map[string]string)
		for _, v := range ev.Fields {
			evData[v.Name] = v.Content
		}
		for k, v := range evData {
			if mappings[k] {
				evDetails[k] = v
				delete(evData, k)
			}
		}

		evData["message"] = ev.Text

		evData["value"] = ""
		if event.Data.Recomendation != "" {
			evData["value"] = event.Data.Recomendation
		}

		host := "unknown"
		if evDetails["hostname"] != "" {
			host = evDetails["hostname"]
		}

		resource := "unknown"
		if evDetails["appname"] != "" {
			resource = evDetails["appname"]
		}

		ts := time.Unix(0, ev.Timestamp*int64(time.Millisecond))

		// Now we need to build a fingerprint to help us identify duplicate events
		fingerprintRaw := []string{
			resource,
			host,
			"",
			event.Service,
			"",
			event.Type,
			event.Subject,
		}

		fingerprint, err := fingerprint.Calculate(fingerprintRaw)
		if err != nil {
			return events, fmt.Errorf("unable to calculate fingerprint: %w", err)
		}

		events = append(events, Event{
			Source:         "vrli",
			Type:           event.Type,
			Status:         event.Status,
			Subject:        event.Subject,
			Time:           ts,
			Resource:       resource,
			Service:        event.Service,
			Host:           host,
			Severity:       event.Severity,
			Data:           evData,
			FingerprintRaw: fingerprintRaw,
			Fingerprint:    fingerprint,
		})
	}

	return events, nil
}
