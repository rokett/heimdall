package main

type ruleDefinition struct {
	ID         int64  `db:"id"`
	Definition string `db:"definition"`
}
