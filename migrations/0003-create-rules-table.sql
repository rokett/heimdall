IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'rules')
	CREATE TABLE rules (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        name NVARCHAR(200) NOT NULL,
        definition NVARCHAR(max) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
        CONSTRAINT UniqueName UNIQUE(name),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'rules_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER rules_updated_at ON rules AFTER UPDATE AS
		UPDATE rules
		SET updated_at = GETUTCDATE()
		FROM rules INNER JOIN deleted d
		ON rules.id = d.id;');
