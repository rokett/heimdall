IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'events')
	CREATE TABLE events (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        source NVARCHAR(100),
        type NVARCHAR(100),
        status NVARCHAR(50),
        lifetime_count INT,
        recurrence_count INT,
        subject NVARCHAR(100),
        service NVARCHAR(100),
        system NVARCHAR(100),
        environment NVARCHAR(100),
        host NVARCHAR(100),
        resource NVARCHAR(100),
        data NVARCHAR(MAX),
        checksum NVARCHAR(64) NOT NULL DEFAULT '',
        created_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
    );
