IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'event_history')
	CREATE TABLE event_history (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        source NVARCHAR(100),
        type NVARCHAR(100),
        status NVARCHAR(50),
        subject NVARCHAR(100),
        service NVARCHAR(100),
        system NVARCHAR(100),
        environment NVARCHAR(100),
        host NVARCHAR(100),
        resource NVARCHAR(100),
        data NVARCHAR(MAX),
        event_id INT NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETUTCDATE(),
    );
