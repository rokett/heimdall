@echo off
SETLOCAL

set VERSION=0.18.0

REM Set build number from git commit hash
for /f %%i in ('git rev-parse HEAD') do set BUILD=%%i

set LDFLAGS=-ldflags "-X main.version=%VERSION% -X main.build=%BUILD% -s -w -extldflags '-static'"

goto build

:build
    echo "=== Building executable ==="
    set GOARCH=amd64

    go build -a -mod=vendor %LDFLAGS% -o heimdalld.exe .\cmd\heimdalld

    goto :clean

:clean
    set VERSION=
    set BUILD=
    set LDFLAGS=
    set GOARCH=
    set GOOS=

    goto :EOF
